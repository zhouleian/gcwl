var db = require('../database.js');

exports.dbCon = function(sql,callback){
    db.mysqlPool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, function(err,results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false,results);
            connection.release();
        });
    });
}

exports.dbCon2 = function(sql,callback){
    db.mysqlPool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, function(err) {
            if (err) {
                callback(true);
                return;
            }
            callback(false);
            connection.release();
        });
    });
}
