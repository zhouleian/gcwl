/* 
* @Author: chenhao
* @Date:   2015-06-11 12:58:12
* @Last Modified by:   chenhao
* @Last Modified time: 2015-07-03 14:04:19
*/
//查询
exports.getReqSqlByQeury = function(data, callback){
    var sql = "";
    for(var key in data){
        sql += " and " + key + " = '" + data[key] + "' ";    
    }
    callback(sql);
};
//条件查询
exports.getReqSqlByQeury2 = function(data, callback){
    var sql = "";
    for(var key in data){
        if(sql.length == 0){
            sql +=  " and " + key + " like  '%" + data[key] + "%' ";
        }else{
            sql += " or " + key + " like  '%" + data[key] + "%' ";
        }
    }
    callback(sql);
};

//插入
exports.getReqSqlByInsert = function(data, callback){
    var sql = "";
    for(var key in data){
        if(sql.length == 0){
            sql += key + " = '" + data[key] + "' ";  
        }else{
            sql += " , " + key + " = '" + data[key] + "' ";  
        }
    }
    callback(sql);
};

//批量插入集装箱和订单的关系
exports.getReqSqlByInsert2 = function(data, callback){
  //  console.log(data); // 测试
    var sql = "";
    for(var i= 0;i< data.length-1;i++){
        if(i == data.length-2){
            sql+="( '" +data[i] +"' , '"+data[data.length-1] +" ')";
        }
        else {
            sql+="( '" +data[i] +"' , '"+data[data.length-1] +" '),";
        }
    }
    callback(sql);
};

//批量插入路线和站点的关系
exports.getReqSqlByInsert3 = function(data, callback){
    console.log(data);
    var sql = "";
    for(var i= 0;i< data.length-1;i=i+2){
        if(i == data.length-3){
            sql+="( '" +data[i] +"' , '" +data[i+1]+"','" +data[data.length-1] +" ')";
        }
        else {
            sql+="( '" +data[i] +"' , '"+data[i+1]+"','" +data[data.length-1] +" '),";
        }
    }
    callback(sql);
};

//更新
exports.getReqSqlByUpdate = function(data, callback){
    var sql = "";
    for(var key in data){
        if(sql.length == 0){
            sql += key + " = '" + data[key] + "' ";
        }else{
            sql += " , " + key + " = '" + data[key] + "' ";
        }
    }
    callback(sql);
};
//删除
exports.getReqSqlByDelete = function(data, callback){
    var sql = "(";
    sql += data.toString();
    sql += ")";
    callback(sql);
};

