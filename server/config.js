/**
 * Created by Administrator on 2017/6/22 0022.
 */
module.exports =
{
    mysql: {
        host: '127.0.0.1',
        user: 'root',
        password: 'admin',
        database:'test',
        port: 3306,
        connectionLimit: 10,
        dateStrings:true,
        supportBigNumbers: true
    }
};
