/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var containerService = require('../service/containerService.js');
var router = express.Router();

//查询集装箱
router.post("/container/query", function(req, res){
    var data = req.body;
    containerService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询集装箱
router.post("/container/queryById", function(req, res){
    var data = req.body;
    containerService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//条件查询
router.post("/container/query2", function(req, res){
    var data = req.body;
    containerService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增集装箱
router.post("/container/insert", function(req, res){
    var data = req.body;
    containerService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新集装箱
router.post("/container/update", function(req, res){
    var data = req.body;
    containerService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除集装箱
router.post("/container/delete", function(req, res){
    var data = req.body;
    containerService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;