/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var gpsInformationService = require('../service/gpsInformationService.js');
var router = express.Router();

//查询物流信息
router.post("/gpsInformation/query", function(req, res){
    var data = req.body;
    gpsInformationService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询物流信息
router.post("/gpsInformation/queryById", function(req, res){
    var data = req.body;
    gpsInformationService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/gpsInformation/query2", function(req, res){
    var data = req.body;
    gpsInformationService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增物流信息
router.post("/gpsInformation/insert", function(req, res){
    var data = req.body;
    gpsInformationService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新物流信息
router.post("/gpsInformation/update", function(req, res){
    var data = req.body;
    gpsInformationService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除物流信息
router.post("/gpsInformation/delete", function(req, res){
    var data = req.body;
    gpsInformationService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;