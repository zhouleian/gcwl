/**
 * Created by Zhangyi on 2017/6/29 0029.
 */
var express = require('express');
var collectingOrderService = require('../service/collectingOrderService.js');
var router = express.Router();

//查询代采订单详细信息
router.post("/collectingOrder/query", function(req, res){
    var data = req.body;
    collectingOrderService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询代采订单详细信息
router.post("/collectingOrder/queryById", function(req, res){
    var data = req.body;
    collectingOrderService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/collectingOrder/query2", function(req, res){
    var data = req.body;
    collectingOrderService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增代采订单详细信息
router.post("/collectingOrder/insert", function(req, res){
    var data = req.body;
    collectingOrderService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新代采订单详细信息
router.post("/collectingOrder/update", function(req, res){
    var data = req.body;
    collectingOrderService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除代采订单详细信息
router.post("/collectingOrder/delete", function(req, res){
    var data = req.body;
    collectingOrderService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;