/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var newTransportService = require('../service/newTransportService.js');
var router = express.Router();

//查询客户新增线路
router.post("/newTransport/query", function(req, res){
    var data = req.body;
    newTransportService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询客户新增线路
router.post("/newTransport/queryById", function(req, res){
    var data = req.body;
    newTransportService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/newTransport/query2", function(req, res){
    var data = req.body;
    newTransportService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增客户新增线路
router.post("/newTransport/insert", function(req, res){
    var data = req.body;
    newTransportService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新客户新增线路
router.post("/newTransport/update", function(req, res){
    var data = req.body;
    newTransportService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除客户新增线路
router.post("/newTransport/delete", function(req, res){
    var data = req.body;
    newTransportService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;