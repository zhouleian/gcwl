/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var driverService = require('../service/driverService.js');
var router = express.Router();

//查询司机
router.post("/driver/query", function(req, res){
    var data = req.body;
    driverService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询司机
router.post("/driver/queryById", function(req, res){
    var data = req.body;
    driverService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询用户
router.post("/driver/query2", function(req, res){
    var data = req.body;
    driverService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增司机
router.post("/driver/insert", function(req, res){
    var data = req.body;
    driverService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新司机
router.post("/driver/update", function(req, res){
    var data = req.body;
    driverService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除司机
router.post("/driver/delete", function(req, res){
    var data = req.body;
    driverService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;