/**
 * Created by Zhangyi on 2017/7/8 0029.
 */
var express = require('express');
var stationService = require('../service/stationService.js');
var router = express.Router();

//查询站点信息
router.post("/station/query", function(req, res){
    var data = req.body;
    stationService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询站点信息
router.post("/station/queryById", function(req, res){
    var data = req.body;
    stationService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//条件查询
router.post("/station/query2", function(req, res){
    var data = req.body;
    stationService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增站点信息
router.post("/station/insert", function(req, res){
    var data = req.body;
    stationService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新站点信息
router.post("/station/update", function(req, res){
    var data = req.body;
    stationService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除站点信息
router.post("/station/delete", function(req, res){
    var data = req.body;
    stationService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;