/**
 * Created by Zhangyi on 2017/7/6 0023.
 */


var express = require('express');
var rentConService = require('../service/rentConService.js');
var router = express.Router();

//查询集装箱租赁
router.post("/rentCon/queryById", function(req, res){
    var data = req.body;
    rentConService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//查询集装箱租赁
router.post("/rentCon/query", function(req, res){
    var data = req.body;
    rentConService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询用户
router.post("/rentCon/query2", function(req, res){
    var data = req.body;
    rentConService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//插入集装箱租赁
router.post("/rentCon/insert", function(req, res){
    var data = req.body;
    rentConService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//批量插入集装箱租赁
router.post("/rentCon/insert2", function(req, res){
    var data = req.body;
    rentConService.insert2(data, function(err){
        if(err){
            res.json({msg: '分配失败'});
            return;
        }
        res.json({msg: '分配成功'});
    });
});

//更新集装箱租赁
router.post("/rentCon/update", function(req, res){
    var data = req.body;
    rentConService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//修改集装箱租赁状态为空
router.post("/rentCon/update2", function(req, res){
    var data = req.body;
    rentConService.update2(data, function(err){
        if(err){
            res.json({msg: '置空失败'});
            return;
        }
        res.json({msg: '置空成功'});
    });
});

//修改集装箱租赁状态为租赁中
router.post("/rentCon/update3", function(req, res){
    var data = req.body;
    rentConService.update3(data, function(err){
        if(err){
            res.json({msg: '分配失败'});
            return;
        }
        res.json({msg: '分配成功'});
    });
});

//删除集装箱租赁
router.post("/rentCon/delete", function(req, res){
    var data = req.body;
    rentConService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;