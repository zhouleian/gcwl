/**
 * Created by Zhangyi on 2017/6/29 0029.
 */
var express = require('express');
var pathInfService = require('../service/pathInfService.js');
var router = express.Router();

//查询路线站点详细信息
router.post("/pathInf/query", function(req, res){
    var data = req.body;
    pathInfService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询路线站点详细信息
router.post("/pathInf/queryById", function(req, res){
    var data = req.body;
    pathInfService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/pathInf/query2", function(req, res){
    var data = req.body;
    pathInfService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//批量插入站点路线关系
router.post("/pathInf/insert2", function(req, res){
    var data = req.body;
    pathInfService.insert2(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//新增路线站点详细信息
router.post("/pathInf/insert", function(req, res){
    var data = req.body;
    pathInfService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新路线站点详细信息
router.post("/pathInf/update", function(req, res){
    var data = req.body;
    pathInfService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除路线站点详细信息
router.post("/pathInf/delete", function(req, res){
    var data = req.body;
    pathInfService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;