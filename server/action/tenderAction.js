/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var tenderService = require('../service/tenderService.js');
var router = express.Router();

//查询招标信息
router.post("/tender/query", function(req, res){
    var data = req.body;
    tenderService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询招标信息
router.post("/tender/queryById", function(req, res){
    var data = req.body;
    tenderService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//条件查询
router.post("/tender/query2", function(req, res){
    var data = req.body;
    tenderService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增招标信息
router.post("/tender/insert", function(req, res){
    var data = req.body;
    tenderService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新招标信息
router.post("/tender/update", function(req, res){
    var data = req.body;
    tenderService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除招标信息
router.post("/tender/delete", function(req, res){
    var data = req.body;
    tenderService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;