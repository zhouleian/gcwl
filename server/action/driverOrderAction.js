/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var driverOrderService = require('../service/driverOrderService.js');
var router = express.Router();

//查询司机订单
router.post("/driverOrder/query", function(req, res){
    var data = req.body;
    driverOrderService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询司机订单
router.post("/driverOrder/queryById", function(req, res){
    var data = req.body;
    driverOrderService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/driverOrder/query2", function(req, res){
    var data = req.body;
    driverOrderService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增司机订单
router.post("/driverOrder/insert", function(req, res){
    var data = req.body;
    driverOrderService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新司机订单
router.post("/driverOrder/update", function(req, res){
    var data = req.body;
    driverOrderService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除司机订单
router.post("/driverOrder/delete", function(req, res){
    var data = req.body;
    driverOrderService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;