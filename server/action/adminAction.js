/**
 * Created by Administrator on 2017/6/23 0023.
 */


var express = require('express');
var adminService = require('../service/adminService.js');
var router = express.Router();

//查询管理员
router.post("/admin/queryRole", function(req, res){
    var data = req.body;
    adminService.queryRole(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询管理员
router.post("/admin/queryByRoleId", function(req, res){
    var data = req.body;
    adminService.queryByRoleId(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//查询管理员
router.post("/admin/query", function(req, res){
    var data = req.body;
    adminService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询用户
router.post("/admin/query2", function(req, res){
    var data = req.body;
    adminService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//插入管理员
router.post("/admin/insert", function(req, res){
    var data = req.body;
    adminService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新管理员
router.post("/admin/update", function(req, res){
    var data = req.body;
    adminService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除管理员
router.post("/admin/delete", function(req, res){
    var data = req.body;
    adminService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;