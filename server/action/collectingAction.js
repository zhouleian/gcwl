/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var collectingService = require('../service/collectingService.js');
var router = express.Router();

//查询代采订单
router.post("/collecting/query", function(req, res){
    var data = req.body;
    collectingService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询代采订单
router.post("/collecting/queryById", function(req, res){
    var data = req.body;
    collectingService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/collecting/query2", function(req, res){
    var data = req.body;
    collectingService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增代采订单
router.post("/collecting/insert", function(req, res){
    var data = req.body;
    collectingService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新代采订单
router.post("/collecting/update", function(req, res){
    var data = req.body;
    collectingService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除代采订单
router.post("/collecting/delete", function(req, res){
    var data = req.body;
    collectingService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;