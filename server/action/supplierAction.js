/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var supplierService = require('../service/supplierService.js');
var router = express.Router();

//查询代理商信息
router.post("/supplier/query", function(req, res){
    var data = req.body;
    supplierService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询代理商信息
router.post("/supplier/queryById", function(req, res){
    var data = req.body;
    supplierService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/supplier/query2", function(req, res){
    var data = req.body;
    supplierService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增代理商信息
router.post("/supplier/insert", function(req, res){
    var data = req.body;
    supplierService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新代理商信息
router.post("/supplier/update", function(req, res){
    var data = req.body;
    supplierService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除代理商信息
router.post("/supplier/delete", function(req, res){
    var data = req.body;
    supplierService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;