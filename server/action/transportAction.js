/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var transportService = require('../service/transportService.js');
var router = express.Router();

//查询运输订单
router.post("/transport/query", function(req, res){
    var data = req.body;
    transportService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询运输订单
router.post("/transport/queryById", function(req, res){
    var data = req.body;
    transportService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/transport/query2", function(req, res){
    var data = req.body;
    transportService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增运输订单
router.post("/transport/insert", function(req, res){
    var data = req.body;
    transportService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新运输订单
router.post("/transport/update", function(req, res){
    var data = req.body;
    transportService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除运输订单
router.post("/transport/delete", function(req, res){
    var data = req.body;
    transportService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;