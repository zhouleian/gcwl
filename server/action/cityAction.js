/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var cityService = require('../service/cityService.js');
var router = express.Router();

//查询city
router.post("/city/query", function(req, res){
    var data = req.body;
    cityService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询provice
router.post("/provice/query", function(req, res){
    var data = req.body;
    cityService.queryProvice(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/city/queryById", function(req, res){
    var data = req.body;
    cityService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增city
router.post("/city/insert", function(req, res){
    var data = req.body;
    cityService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新city
router.post("/city/update", function(req, res){
    var data = req.body;
    cityService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除city
router.post("/city/delete", function(req, res){
    var data = req.body;
    cityService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;