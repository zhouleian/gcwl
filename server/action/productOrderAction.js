/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var productOrderService = require('../service/productOrderService.js');
var router = express.Router();

//查询产品订单
router.post("/productOrder/query", function(req, res){
    var data = req.body;
    productOrderService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询产品订单
router.post("/productOrder/queryById", function(req, res){
    var data = req.body;
    productOrderService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/productOrder/query2", function(req, res){
    var data = req.body;
    productOrderService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增产品订单
router.post("/productOrder/insert", function(req, res){
    var data = req.body;
    productOrderService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新产品订单
router.post("/productOrder/update", function(req, res){
    var data = req.body;
    productOrderService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除产品订单
router.post("/productOrder/delete", function(req, res){
    var data = req.body;
    productOrderService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;