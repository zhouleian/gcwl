/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var highwayService = require('../service/highwayService.js');
var router = express.Router();

//查询公路计费
router.post("/highway/query", function(req, res){
    var data = req.body;
    highwayService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/highway/query2", function(req, res){
    var data = req.body;
    highwayService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增公路计费
router.post("/highway/insert", function(req, res){
    var data = req.body;
    highwayService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新公路计费
router.post("/highway/update", function(req, res){
    var data = req.body;
    highwayService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除公路计费
router.post("/highway/delete", function(req, res){
    var data = req.body;
    highwayService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;