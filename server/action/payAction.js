/**
 * Created by Zhangyi on 2017/7/6 0023.
 */


var express = require('express');
var payService = require('../service/payService.js');
var router = express.Router();

//查询付款信息
router.post("/pay/queryById", function(req, res){
    var data = req.body;
    payService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//查询付款信息
router.post("/pay/query", function(req, res){
    var data = req.body;
    payService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询用户
router.post("/pay/query2", function(req, res){
    var data = req.body;
    payService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//插入付款信息
router.post("/pay/insert", function(req, res){
    var data = req.body;
    payService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新付款信息
router.post("/pay/update", function(req, res){
    var data = req.body;
    payService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除付款信息
router.post("/pay/delete", function(req, res){
    var data = req.body;
    payService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;