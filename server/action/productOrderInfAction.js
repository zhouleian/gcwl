/**
 * Created by Zhangyi on 2017/6/29 0029.
 */
var express = require('express');
var productOrderInfService = require('../service/productOrderInfService.js');
var router = express.Router();

//查询产品订单详细信息
router.post("/productOrderInf/query", function(req, res){
    var data = req.body;
    productOrderInfService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询产品订单详细信息
router.post("/productOrderInf/queryById", function(req, res){
    var data = req.body;
    productOrderInfService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/productOrderInf/query2", function(req, res){
    var data = req.body;
    productOrderInfService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增产品订单详细信息
router.post("/productOrderInf/insert", function(req, res){
    var data = req.body;
    productOrderInfService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新产品订单详细信息
router.post("/productOrderInf/update", function(req, res){
    var data = req.body;
    productOrderInfService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除产品订单详细信息
router.post("/productOrderInf/delete", function(req, res){
    var data = req.body;
    productOrderInfService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;