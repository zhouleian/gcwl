/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var pathService = require('../service/pathService.js');
var router = express.Router();

//查询铁路路线
router.post("/path/query", function(req, res){
    var data = req.body;
    pathService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询铁路路线
router.post("/path/queryByStationId", function(req, res){
    var data = req.body;
    pathService.queryByStationId(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/path/query2", function(req, res){
    var data = req.body;
    pathService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增铁路路线
router.post("/path/insert", function(req, res){
    var data = req.body;
    pathService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新铁路路线
router.post("/path/update", function(req, res){
    var data = req.body;
    pathService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除铁路路线
router.post("/path/delete", function(req, res){
    var data = req.body;
    pathService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;