/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var rentOrderService = require('../service/rentOrderService.js');
var router = express.Router();

//查询租箱订单
router.post("/rentOrder/query", function(req, res){
    var data = req.body;
    rentOrderService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询租箱订单
router.post("/rentOrder/queryById", function(req, res){
    var data = req.body;
    rentOrderService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/rentOrder/query2", function(req, res){
    var data = req.body;
    rentOrderService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增租箱订单
router.post("/rentOrder/insert", function(req, res){
    var data = req.body;
    rentOrderService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新租箱订单
router.post("/rentOrder/update", function(req, res){
    var data = req.body;
    rentOrderService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除租箱订单
router.post("/rentOrder/delete", function(req, res){
    var data = req.body;
    rentOrderService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;