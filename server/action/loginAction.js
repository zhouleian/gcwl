/* 
* @Author: zhangyi
* @Date:   2017-07-8
*/

var express = require('express');
var adminService = require('../service/adminService.js');
var router = express.Router();

//登录操作
router.post("/query", function(req, res){
    var data = {admin_name: req.body.admin_name, password: req.body.password};
    adminService.query(data, function(err, results){
        if(err || results.length == 0){
            res.redirect("/indexerror");
            return;
        }
       req.session.admin = results[0];
        res.redirect("/index");
    });
});


module.exports = router;