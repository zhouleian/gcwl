/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var inventoryService = require('../service/inventoryService.js');
var router = express.Router();

//查询供应商库存信息信息
router.post("/inventory/query", function(req, res){
    var data = req.body;
    inventoryService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});


//查询供应商库存信息信息
router.post("/inventory/queryById", function(req, res){
    var data = req.body;
    inventoryService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//条件查询
router.post("/inventory/query2", function(req, res){
    var data = req.body;
    inventoryService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增供应商库存信息信息
router.post("/inventory/insert", function(req, res){
    var data = req.body;
    inventoryService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新供应商库存信息信息
router.post("/inventory/update", function(req, res){
    var data = req.body;
    inventoryService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除供应商库存信息信息
router.post("/inventory/delete", function(req, res){
    var data = req.body;
    inventoryService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;