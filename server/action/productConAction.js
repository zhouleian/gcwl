/**
 * Created by Zhangyi on 2017/7/6 0023.
 */


var express = require('express');
var productConService = require('../service/productConService.js');
var router = express.Router();

//查询产品集装箱关系
router.post("/productCon/queryById", function(req, res){
    var data = req.body;
    productConService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});
//查询产品集装箱关系
router.post("/productCon/query", function(req, res){
    var data = req.body;
    productConService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询用户
router.post("/productCon/query2", function(req, res){
    var data = req.body;
    productConService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//插入产品集装箱关系
router.post("/productCon/insert", function(req, res){
    var data = req.body;
    productConService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//批量插入集装箱产品关系
router.post("/productCon/insert2", function(req, res){
    var data = req.body;
    productConService.insert2(data, function(err){
        if(err){
            res.json({msg: '分配失败'});
            return;
        }
        res.json({msg: '分配成功'});
    });
});

//更新产品集装箱关系
router.post("/productCon/update", function(req, res){
    var data = req.body;
    productConService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//更新产品集装箱运输中的状态为空
router.post("/productCon/update2", function(req, res){
    var data = req.body;
    productConService.update2(data, function(err){
        if(err){
            res.json({msg: '置空失败'});
            return;
        }
        res.json({msg: '置空成功'});
    });
});

//修改集装箱租赁状态为租赁中
router.post("/productCon/update3", function(req, res){
    var data = req.body;
    productConService.update3(data, function(err){
        if(err){
            res.json({msg: '分配失败'});
            return;
        }
        res.json({msg: '分配成功'});
    });
});

//删除产品集装箱关系
router.post("/productCon/delete", function(req, res){
    var data = req.body;
    productConService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;