/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var transportTaskService = require('../service/transportTaskService.js');
var router = express.Router();

//查询运输任务订单
router.post("/transportTask/query", function(req, res){
    var data = req.body;
    transportTaskService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//查询运输任务订单
router.post("/transportTask/queryById", function(req, res){
    var data = req.body;
    transportTaskService.queryById(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/transportTask/query2", function(req, res){
    var data = req.body;
    transportTaskService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增运输任务订单
router.post("/transportTask/insert", function(req, res){
    var data = req.body;
    transportTaskService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新运输任务订单
router.post("/transportTask/update", function(req, res){
    var data = req.body;
    transportTaskService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除运输任务订单
router.post("/transportTask/delete", function(req, res){
    var data = req.body;
    transportTaskService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;