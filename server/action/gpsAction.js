/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var gpsService = require('../service/gpsService.js');
var router = express.Router();

//查询GPS
router.post("/gps/query", function(req, res){
    var data = req.body;
    gpsService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/gps/query2", function(req, res){
    var data = req.body;
    gpsService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增GPS
router.post("/gps/insert", function(req, res){
    var data = req.body;
    gpsService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新GPS
router.post("/gps/update", function(req, res){
    var data = req.body;
    gpsService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除GPS
router.post("/gps/delete", function(req, res){
    var data = req.body;
    gpsService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;