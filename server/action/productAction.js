/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var router = express.Router();
var formidable = require("formidable");
var fs = require('fs');
var productService = require('../service/productService.js');
var AVATAR_UPLOAD_FOLDER = '/avatar/';

//查询产品
router.post("/product/query", function(req, res){
    var data = req.body;
    productService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/product/query2", function(req, res){
    var data = req.body;
    productService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增产品
router.post("/product/insert", function(req, res){

    var form = new formidable.IncomingForm(); //创建上传表单
    form.encoding = 'utf-8'; //设置编辑
    form.uploadDir = 'public' + AVATAR_UPLOAD_FOLDER; //设置上传目录
    form.keepExtensions = true; //保留后缀
    form.maxFieldsSize = 2 * 1024 * 1024; //文件大小

    form.parse(req, function(err, fields, files) {
//        var data = fields;
        if (err) {
            res.locals.error = err;
            res.redirect('/index#/product');
            return;
        }

        var extName = ''; //后缀名
      //  console.log("file 的发的发" + files.fulAvatar.type);
        switch (files.fulAvatar.type) {
            case 'image/pjpeg':
                extName = 'jpg';
                break;
            case 'image/jpeg':
                extName = 'jpg';
                break;
            case 'image/png':
                extName = 'png';
                break;
            case 'image/x-png':
                extName = 'png';
                break;
        }

        if(extName.length == 0){
            res.locals.error = '只支持png和jpg格式图片';
            res.redirect('/index#/product');
//            res.render('/product');
            return;
        }
            var avatarName = Math.random() +"."  + extName;
            var newPath = form.uploadDir + avatarName;
           // console.log(newPath);
            fs.renameSync(files.fulAvatar.path, newPath); //重命名
            res.locals.success = '上传成功';
//            data.product_picture = newPath;
            var data = fields;
            data.product_picture = avatarName;

        //console.log(data);
        productService.insert(data, function(err){
            if(err){
               res.json({msg: '新增失败'});
//               res.redirect('/index#/product');
                return;
            }
           // res.json({msg: '新增成功'});
            res.redirect('/index#/product');
        });
    });


});

//更新产品
router.post("/product/update", function(req, res){
    var data = req.body;
    productService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除产品
router.post("/product/delete", function(req, res){
    var data = req.body;
    productService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;