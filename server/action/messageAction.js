/**
 * Created by Administrator on 2017/6/29 0029.
 */
var express = require('express');
var messageService = require('../service/messageService.js');
var router = express.Router();

//查询信息
router.post("/message/query", function(req, res){
    var data = req.body;
    messageService.query(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//条件查询
router.post("/message/query2", function(req, res){
    var data = req.body;
    messageService.query2(data, function(err, results){
        if(err){
            res.json({msg: '查询失败'});
            return;
        }
        res.json(results);
    });
});

//新增信息
router.post("/message/insert", function(req, res){
    var data = req.body;
    messageService.insert(data, function(err){
        if(err){
            res.json({msg: '新增失败'});
            return;
        }
        res.json({msg: '新增成功'});
    });
});

//更新信息
router.post("/message/update", function(req, res){
    var data = req.body;
    messageService.update(data, function(err){
        if(err){
            res.json({msg: '修改失败'});
            return;
        }
        res.json({msg: '修改成功'});
    });
});

//删除信息
router.post("/message/delete", function(req, res){
    var data = req.body;
    messageService.delete(data, function(err){
        if(err){
            res.json({msg: '删除失败'});
            return;
        }
        res.json({msg: '删除成功'});
    });
});

module.exports = router;