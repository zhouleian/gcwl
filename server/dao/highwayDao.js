/**
 * Created by  on 2017/6/29 0029.
 */
var highwayModel = require('../model/highwayModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询公路里程计费
exports.query = function(data, callback) {
    var sql = highwayModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询公路里程计费: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增公路里程计费
exports.insert = function(data, callback) {
    var sql = highwayModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增公路里程计费: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = highwayModel.query;
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询公路计费: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//修改公路里程计费
exports.update = function(data, callback) {
    var sql = highwayModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + highwayModel.pk + " = " + data[highwayModel.pk];
        console.log("修改公路里程计费: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除公路里程计费
exports.delete = function(data, callback) {
    var sql = highwayModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除公路里程计费: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};