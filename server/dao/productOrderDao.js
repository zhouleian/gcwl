/**
 * Created by transportistrator on 2017/6/29 0029.
 */
var productOrderModel = require('../model/productOrderModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询产品订单
exports.query = function(data, callback) {
    var sql = productOrderModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询产品订单
exports.queryById = function(data, callback) {
    var sql = productOrderModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t5.* FROM ("+productOrderModel.queryById+") t5   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询产品订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增产品订单
exports.insert = function(data, callback) {
    var sql = productOrderModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增产品订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改产品订单
exports.update = function(data, callback) {
    var sql = productOrderModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + productOrderModel.pk + " = " + data[productOrderModel.pk];
        console.log("修改产品订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除产品订单
exports.delete = function(data, callback) {
    var sql = productOrderModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除产品订单: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};