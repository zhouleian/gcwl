/**
 * Created by Zhangyi on 2017/6/29 0029.
 */
var cityModel = require('../model/cityModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询city
exports.query = function(data, callback) {
    var sql = cityModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询city: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询provice
exports.queryProvice = function(data, callback) {
    var sql = cityModel.queryProvice;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询city: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.queryById= function(data, callback) {
    var sql = cityModel.queryById;
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("查询city: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增city
exports.insert = function(data, callback) {
    var sql = cityModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增city: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改city
exports.update = function(data, callback) {
    var sql = cityModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + cityModel.pk + " = " + data[cityModel.pk];
        console.log("修改city: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除city
exports.delete = function(data, callback) {
    var sql = cityModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除city: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};