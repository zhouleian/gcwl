/**
 * Created by Zhangyi on 2017/7/6 0023.
 */
var containerModel = require('../model/containerModel.js');
var rentConModel = require('../model/rentConModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询集装箱租赁
exports.queryById = function(data, callback) {
    var sql =rentConModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询集装箱租赁: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//查询集装箱租赁
exports.query = function(data, callback) {
    var sql = rentConModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询集装箱租赁: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+rentConModel.queryById+") t3   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询集装箱租赁: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增集装箱租赁
exports.insert = function(data, callback) {
    var sql = rentConModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增集装箱租赁: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//批量插入集装箱租赁
exports.insert2 = function(data, callback) {
    var sql = rentConModel.insertById + "(container_id,rent_order_id) VALUES";
    req2Sql.getReqSqlByInsert2(data, function(reqSql){
        sql += reqSql;
        console.log("为租赁订单分配集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改集装箱租赁
exports.update = function(data, callback) {
    var sql = rentConModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + rentConModel.pk + " = " + data[rentConModel.pk];
        console.log("修改集装箱租赁: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改集装箱租赁状态为空
exports.update2 = function(data, callback) {
    var sql = containerModel.update+" container_state = '空' WHERE container_id  IN (SELECT t.container_id FROM rent_container_relation t WHERE t.table_id IN ";
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql+")";
        console.log("置集装箱状态为空: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改集装箱租赁状态为租赁中
exports.update3 = function(data, callback) {
    var sql = containerModel.update+" container_state = '租赁中' WHERE container_id  IN  ";
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("置集装箱状态为租赁中: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};


//删除集装箱租赁
exports.delete = function(data, callback) {
    var sql = rentConModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除集装箱租赁: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};