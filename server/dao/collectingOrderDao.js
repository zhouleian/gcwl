/**
 * Created by zhangyi on 2017/6/29 0029.
 */
var collectingOrderModel = require('../model/collectingOrderModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询代采订单详细信息
exports.query = function(data, callback) {
    var sql = collectingOrderModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询代采订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询代采订单详细信息
exports.queryById = function(data, callback) {
    var sql = collectingOrderModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询代采订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t8.* FROM ("+collectingOrderModel.queryById+") t8   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询物流订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增代采订单详细信息
exports.insert = function(data, callback) {
    var sql = collectingOrderModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增代采订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改代采订单详细信息
exports.update = function(data, callback) {
    var sql = collectingOrderModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + collectingOrderModel.pk + " = " + data[collectingOrderModel.pk];
        console.log("修改代采订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除代采订单详细信息
exports.delete = function(data, callback) {
    var sql = collectingOrderModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除代采订单详细信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};