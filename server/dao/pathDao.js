/**
 * Created by pathistrator on 2017/6/29 0029.
 */
var pathModel = require('../model/pathModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询铁路路线
exports.query = function(data, callback) {
    var sql = pathModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询铁路路线: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询铁路路线
exports.queryByStationId = function(data, callback) {
    var sql = pathModel.queryByStationId;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询铁路路线: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//条件查询
exports.query2 = function(data, callback) {
    var sql ="SELECT t3.* FROM ("+pathModel.queryByStationId+") t3   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询铁路路线: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增铁路路线
exports.insert = function(data, callback) {
    var sql = pathModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增铁路路线: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改铁路路线
exports.update = function(data, callback) {
    var sql = pathModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + pathModel.pk + " = " + data[pathModel.pk];
        console.log("修改铁路路线: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除铁路路线
exports.delete = function(data, callback) {
    var sql = pathModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除铁路路线: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};