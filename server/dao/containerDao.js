/**
 * Created by containeristrator on 2017/6/29 0029.
 */
var containerModel = require('../model/containerModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询集装箱
exports.query = function(data, callback) {
    var sql = containerModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询集装箱
exports.queryById = function(data, callback) {
    var sql = containerModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+ containerModel.queryById +") t3  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增集装箱
exports.insert = function(data, callback) {
    var sql = containerModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改集装箱
exports.update = function(data, callback) {
    var sql = containerModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + containerModel.pk + " = " + data[containerModel.pk];
        console.log("修改集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除集装箱
exports.delete = function(data, callback) {
    var sql = containerModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除集装箱: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};