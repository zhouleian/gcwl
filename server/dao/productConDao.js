/**
 * Created by Zhangyi on 2017/7/6 0023.
 */

var containerModel = require('../model/containerModel.js');
var productConModel = require('../model/productConModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询产品集装箱关系
exports.queryById = function(data, callback) {
    var sql =productConModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品集装箱关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//查询产品集装箱关系
exports.query = function(data, callback) {
    var sql = productConModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品集装箱关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+productConModel.queryById+") t3   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询产品集装箱关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增产品集装箱关系
exports.insert = function(data, callback) {
    var sql = productConModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增产品集装箱关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//批量插入集装箱产品关系
exports.insert2 = function(data, callback) {
    var sql = productConModel.insertById + "(container_id , product_order_id) VALUES";
    req2Sql.getReqSqlByInsert2(data, function(reqSql){
        sql += reqSql;
        console.log("为产品订单分配集装箱: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改产品集装箱关系
exports.update = function(data, callback) {
    var sql = productConModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + productConModel.pk + " = " + data[productConModel.pk];
        console.log("修改产品集装箱关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改产品订单集装箱运输状态为空
exports.update2 = function(data, callback) {
    var sql = containerModel.update+" container_state = '空' WHERE container_id  IN (SELECT t.container_id FROM container_product_order_relation t WHERE t.table_id IN ";
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql+")";
        console.log("置集装箱状态为空: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//修改集装箱租赁状态为运输中
exports.update3 = function(data, callback) {
    var sql = containerModel.update+" container_state = '运输中' WHERE container_id  IN  ";
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("置集装箱状态为运输中: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除产品集装箱关系
exports.delete = function(data, callback) {
    var sql = productConModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除产品集装箱关系: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};