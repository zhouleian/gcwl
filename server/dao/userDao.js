/**
 * Created by Administrator on 2017/6/29 0029.
 */
var userModel = require('../model/userModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询用户
exports.query = function(data, callback) {
    var sql = userModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询用户: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = userModel.query;
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询用户: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增用户
exports.insert = function(data, callback) {
    var sql = userModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增用户: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改用户
exports.update = function(data, callback) {
    var sql = userModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + userModel.pk + " = " + data[userModel.pk];
        console.log("修改用户: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除用户
exports.delete = function(data, callback) {
    var sql = userModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除用户: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};