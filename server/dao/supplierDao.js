/**
 * Created by supplieristrator on 2017/6/29 0029.
 */
var supplierModel = require('../model/supplierModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询代理商信息
exports.query = function(data, callback) {
    var sql = supplierModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询代理商信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询代理商信息
exports.queryById = function(data, callback) {
    var sql = supplierModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询代理商信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t4.* FROM ("+ supplierModel.queryById +") t4  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询代理商信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增代理商信息
exports.insert = function(data, callback) {
    var sql = supplierModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增代理商信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改代理商信息
exports.update = function(data, callback) {
    var sql = supplierModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + supplierModel.pk + " = " + data[supplierModel.pk];
        console.log("修改代理商信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除代理商信息
exports.delete = function(data, callback) {
    var sql = supplierModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除代理商信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};