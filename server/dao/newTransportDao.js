/**
 * Created by transportistrator on 2017/6/29 0029.
 */
var newTransportModel = require('../model/newTransportModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询客户新增线路
exports.query = function(data, callback) {
    var sql = newTransportModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询客户新增线路: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询客户新增线路
exports.queryById = function(data, callback) {
    var sql = newTransportModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询客户新增线路: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t7.* FROM ("+newTransportModel.queryById+") t7   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询客户新增线路: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增客户新增线路
exports.insert = function(data, callback) {
    var sql = newTransportModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增客户新增线路: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改客户新增线路
exports.update = function(data, callback) {
    var sql = newTransportModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + newTransportModel.pk + " = " + data[newTransportModel.pk];
        console.log("修改客户新增线路: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除客户新增线路
exports.delete = function(data, callback) {
    var sql = newTransportModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除客户新增线路: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};