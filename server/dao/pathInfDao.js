/**
 * Created by zhangyi on 2017/6/29 0029.
 */
var pathInfModel = require('../model/pathInfModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询线路站点详细信息
exports.query = function(data, callback) {
    var sql = pathInfModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询线路站点详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询线路站点详细信息
exports.queryById = function(data, callback) {
    var sql = pathInfModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询线路站点详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询线路站点详细信息
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+pathInfModel.queryById+") t3  WHERE 1=1 ";
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql +" ORDER BY path_order";
        console.log("查询线路站点详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增线路站点详细信息
exports.insert = function(data, callback) {
    var sql = pathInfModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增线路站点详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//批量新增线路站点详细信息
exports.insert2 = function(data, callback) {
    var sql = pathInfModel.insertById+"(path_order,station_id,path_id) VALUES";
    req2Sql.getReqSqlByInsert3(data, function(reqSql){
        sql += reqSql;
        console.log("批量新增线路站点关系: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改线路站点详细信息
exports.update = function(data, callback) {
    var sql = pathInfModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + pathInfModel.pk + " = " + data[pathInfModel.pk];
        console.log("修改线路站点详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除线路站点详细信息
exports.delete = function(data, callback) {
    var sql = pathInfModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除线路站点详细信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};