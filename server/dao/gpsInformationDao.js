/**
 * Created by gpsInformationistrator on 2017/6/29 0029.
 */
var gpsInformationModel = require('../model/gpsInformationModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询物流信息
exports.query = function(data, callback) {
    var sql = gpsInformationModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询物流信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询物流信息
exports.queryById = function(data, callback) {
    var sql = gpsInformationModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询物流信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql =  "SELECT t3.* FROM ("+gpsInformationModel.queryById+") t3  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询物流信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增物流信息
exports.insert = function(data, callback) {
    var sql = gpsInformationModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增物流信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改物流信息
exports.update = function(data, callback) {
    var sql = gpsInformationModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + gpsInformationModel.pk + " = " + data[gpsInformationModel.pk];
        console.log("修改物流信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除物流信息
exports.delete = function(data, callback) {
    var sql = gpsInformationModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除物流信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};