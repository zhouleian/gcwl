/**
 * Created by administrator on 2017/6/23 0023.
 */

var driverModel = require('../model/driverModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询司机
exports.queryById = function(data, callback) {
    var sql = driverModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询司机: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询司机
exports.query = function(data, callback) {
    var sql = driverModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询司机: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t4.* FROM ("+ driverModel.queryById +") t4  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询司机: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增司机
exports.insert = function(data, callback) {
    var sql = driverModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增司机: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改司机
exports.update = function(data, callback) {
    var sql = driverModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + driverModel.pk + " = " + data[driverModel.pk];
        console.log("修改司机: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除司机
exports.delete = function(data, callback) {
    var sql = driverModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除司机: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};