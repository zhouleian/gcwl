/**
 * Created by Administrator on 2017/6/23 0023.
 */

var adminModel = require('../model/adminModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询管理员role
exports.queryRole = function(data, callback) {
    var sql = adminModel.queryRole;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询管理员Role: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

exports.selectByAdminName = function(data,callback){
    var sql = adminModel.selectByAdminName;
//    req2Sql.
};

//查询管理员
exports.queryByRoleId = function(data, callback) {
    var sql =adminModel.queryByRoleId;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询管理员queryByRoleid: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//查询管理员//
exports.query = function(data, callback) {
    var sql = adminModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询管理员query: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+adminModel.queryByRoleId+") t3   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询管理员: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增管理员
exports.insert = function(data, callback) {
    var sql = adminModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增管理员: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改管理员
exports.update = function(data, callback) {
    var sql = adminModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + adminModel.pk + " = " + data[adminModel.pk];
        console.log("修改管理员: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除管理员
exports.delete = function(data, callback) {
    var sql = adminModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除管理员: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};