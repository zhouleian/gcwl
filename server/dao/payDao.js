/**
 * Created by payistrator on 2017/6/23 0023.
 */

var payModel = require('../model/payModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询付款信息
exports.queryById = function(data, callback) {
    var sql =payModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询付款信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//查询付款信息
exports.query = function(data, callback) {
    var sql = payModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询付款信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t3.* FROM ("+payModel.queryById+") t3   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询付款信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增付款信息
exports.insert = function(data, callback) {
    var sql = payModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增付款信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改付款信息
exports.update = function(data, callback) {
    var sql = payModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + payModel.pk + " = " + data[payModel.pk];
        console.log("修改付款信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除付款信息
exports.delete = function(data, callback) {
    var sql = payModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除付款信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};