/**
 * Created by zhangyi on 2017/6/29 0029.
 */
var productOrderInfModel = require('../model/productOrderInfModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询产品订单详细信息
exports.query = function(data, callback) {
    var sql = productOrderInfModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询产品订单详细信息
exports.queryById = function(data, callback) {
    var sql = productOrderInfModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t8.* FROM ("+productOrderInfModel.queryById+") t8   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询产品订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增产品订单详细信息
exports.insert = function(data, callback) {
    var sql = productOrderInfModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增产品订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改产品订单详细信息
exports.update = function(data, callback) {
    var sql = productOrderInfModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + productOrderInfModel.pk + " = " + data[productOrderInfModel.pk];
        console.log("修改产品订单详细信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除产品订单详细信息
exports.delete = function(data, callback) {
    var sql = productOrderInfModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除产品订单详细信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};