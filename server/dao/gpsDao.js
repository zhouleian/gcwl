/**
 * Created by gpsistrator on 2017/6/29 0029.
 */
var gpsModel = require('../model/gpsModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询GPS
exports.query = function(data, callback) {
    var sql = gpsModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询GPS: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = gpsModel.query;
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询GPS: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增GPS
exports.insert = function(data, callback) {
    var sql = gpsModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增GPS: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改GPS
exports.update = function(data, callback) {
    var sql = gpsModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + gpsModel.pk + " = " + data[gpsModel.pk];
        console.log("修改GPS: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除GPS
exports.delete = function(data, callback) {
    var sql = gpsModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除GPS: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};