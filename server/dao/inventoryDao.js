/**
 * Created by inventoryistrator on 2017/6/29 0029.
 */
var inventoryModel = require('../model/inventoryModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询供应商库存信息
exports.query = function(data, callback) {
    var sql = inventoryModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询供应商库存信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询供应商库存信息
exports.queryById = function(data, callback) {
    var sql = inventoryModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询供应商库存信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t4.* FROM ("+ inventoryModel.queryById +") t4  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询供应商库存信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增供应商库存信息
exports.insert = function(data, callback) {
    var sql = inventoryModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增供应商库存信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改供应商库存信息
exports.update = function(data, callback) {
    var sql = inventoryModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + inventoryModel.pk + " = " + data[inventoryModel.pk];
        console.log("修改供应商库存信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除供应商库存信息
exports.delete = function(data, callback) {
    var sql = inventoryModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除供应商库存信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};