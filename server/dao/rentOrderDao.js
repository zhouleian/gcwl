/**
 * Created by transportistrator on 2017/6/29 0029.
 */
var rentOrderModel = require('../model/rentOrderModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");

//查询租箱订单
exports.query = function(data, callback) {
    var sql = rentOrderModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询租箱订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询租箱订单
exports.queryById = function(data, callback) {
    var sql = rentOrderModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询租箱订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};


//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t5.* FROM ("+rentOrderModel.queryById+") t5  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询租箱订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增租箱订单
exports.insert = function(data, callback) {
    var sql = rentOrderModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增租箱订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改租箱订单
exports.update = function(data, callback) {
    var sql = rentOrderModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + rentOrderModel.pk + " = " + data[rentOrderModel.pk];
        console.log("修改租箱订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除租箱订单
exports.delete = function(data, callback) {
    var sql = rentOrderModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除租箱订单: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};