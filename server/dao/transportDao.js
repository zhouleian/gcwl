/**
 * Created by transportistrator on 2017/6/29 0029.
 */
var transportModel = require('../model/transportModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询运输订单
exports.query = function(data, callback) {
    var sql = transportModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询运输订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//查询运输订单
exports.queryById = function(data, callback) {
    var sql = transportModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询运输订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t5.* FROM ("+transportModel.queryById+") t5   WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询物流订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增运输订单
exports.insert = function(data, callback) {
    var sql = transportModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增运输订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改运输订单
exports.update = function(data, callback) {
    var sql = transportModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + transportModel.pk + " = " + data[transportModel.pk];
        console.log("修改运输订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除运输订单
exports.delete = function(data, callback) {
    var sql = transportModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除运输订单: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};