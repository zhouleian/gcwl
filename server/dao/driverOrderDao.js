/**
 * Created by transportistrator on 2017/6/29 0029.
 */
var driverOrderModel = require('../model/driverOrderModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询司机订单
exports.query = function(data, callback) {
    var sql = driverOrderModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询司机订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};
//查询司机订单
exports.queryById = function(data, callback) {
    var sql = driverOrderModel.queryById;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询司机订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};


//条件查询
exports.query2 = function(data, callback) {
    var sql = "SELECT t4.* FROM ("+driverOrderModel.queryById+") t4  WHERE 1=1";
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询司机订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增司机订单
exports.insert = function(data, callback) {
    var sql = driverOrderModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增司机订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改司机订单
exports.update = function(data, callback) {
    var sql = driverOrderModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + driverOrderModel.pk + " = " + data[driverOrderModel.pk];
        console.log("修改司机订单: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除司机订单
exports.delete = function(data, callback) {
    var sql = driverOrderModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除司机订单: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};