/**
 * Created by productistrator on 2017/6/29 0029.
 */
var productModel = require('../model/productModel.js');
var req2Sql = require('../util/req2Sql.js');
var dbCon = require("../util/dbConnection.js");
//查询产品
exports.query = function(data, callback) {
    var sql = productModel.query;
    req2Sql.getReqSqlByQeury(data, function(reqSql){
        sql += reqSql;
        console.log("查询产品信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//条件查询
exports.query2 = function(data, callback) {
    var sql = productModel.query;
    req2Sql.getReqSqlByQeury2(data, function(reqSql){
        sql += reqSql;
        console.log("条件查询产品信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon(sql,callback);
    });
};

//新增产品
exports.insert = function(data, callback) {
    var sql = productModel.insert;
    req2Sql.getReqSqlByInsert(data, function(reqSql){
        sql += reqSql;
        console.log("新增产品信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);

    });

};

//修改产品
exports.update = function(data, callback) {
    var sql = productModel.update;
    req2Sql.getReqSqlByUpdate(data, function(reqSql){
        sql += reqSql;
        sql += " WHERE " + productModel.pk + " = " + data[productModel.pk];
        console.log("修改产品信息: " + sql);
        // get a connection from the pool
        dbCon.dbCon2(sql,callback);
    });
};

//删除产品
exports.delete = function(data, callback) {
    var sql = productModel.delete;
    req2Sql.getReqSqlByDelete(data, function(reqSql){
        sql += reqSql;
        console.log("删除产品信息: " + sql);
        dbCon.dbCon2(sql,callback);
    });
};