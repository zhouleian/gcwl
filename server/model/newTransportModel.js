/**
 * Created by Administrator on 2017/6/23 0023.
 */
var new_transport = {
    transport_id : "",    //信息编号
    user_id : "",              //用户编号
    start_provice : "",  //发货省份
    start_city:"",     //发货城市
    start_station: "",      //发货火车站
    start_address:"",       //发货详细信息
    end_provice : "",  //收货省份
    end_city:"",     //收货城市
    end_station: "",      //收货火车站
    end_address:"",       //收货详细信息
    queryById: "SELECT t1.*,t2.user_name,t3.provice_name AS provice1,t4.city_name AS city1,t5.provice_name AS provice2,t6.city_name AS city2 FROM new_transport t1 INNER JOIN users t2 ON t1.user_id = t2.user_id INNER JOIN  provice t3 ON t1.start_provice = t3.provice_id INNER JOIN  city t4 ON t1.start_city = t4.city_id INNER JOIN  provice t5 ON t1.end_provice = t5.provice_id INNER JOIN  city t6 ON t1.end_city = t6.city_id",
    query: "SELECT * FROM new_transport WHERE 1=1 ",
    insert: "INSERT INTO new_transport SET ",
    update: "UPDATE new_transport SET ",
    delete: "DELETE FROM new_transport WHERE transport_id in ",
    pk: "transport_id"
}

module.exports = new_transport;