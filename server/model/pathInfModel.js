/**
 * Created by zhangyi on 2017/7/6 0023.
 */
var pathInf = {
    table_id : "",    //主键
    station_id : "",  //站点编号
    path_id: "",  //路线编号
    path_order:"",     //顺序
    queryById: "SELECT t1.* ,t2.station_name FROM station_path_relation t1  INNER JOIN station t2 ON t1.station_id = t2.station_id",
    insertById:"INSERT INTO station_path_relation ",
    query: "SELECT * FROM station_path_relation WHERE 1=1 ",
    insert: "INSERT INTO station_path_relation SET ",
    update: "UPDATE station_path_relation SET ",
    delete: "DELETE FROM station_path_relation WHERE table_id in ",
    pk: "table_id"
}

module.exports = pathInf;