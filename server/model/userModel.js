/**
 * Created by Administrator on 2017/6/23 0023.
 */
var user = {
    user_id : "",    //用户编号
    user_name : "",  //用户名
    password : "",  //密码
    company_name:"",     //用户公司
    company_address: "", //公司地址
    company_id:"",      //营业执照（图片路径）
    user_id_number:"", //法人证明（图片路径）
    email:"",           //邮箱
    qq:"",               //QQ
    tel1:"",              //手机号
    tel2:"",     //电话
    query: "SELECT * FROM users WHERE 1=1 ",
    insert: "INSERT INTO users SET ",
    update: "UPDATE users SET ",
    delete: "DELETE FROM users WHERE user_id in ",
    pk: "user_id"
}

module.exports = user;