/**
 * Created by Zhangyi on 2017/7/8 0029.
 */
var station = {
    station_id : "",    //站点编号
    station_name:"",    //站点名称
    provice_id : "",  //省
    city_id : "",  //市
    longitude:"", //经度
    latitude : "",  //纬度
    queryById:"SELECT t1.*,t2.city_name ,t3.provice_name  FROM station t1 INNER JOIN city t2 ON t1.city_id = t2.city_id INNER JOIN  provice t3 ON t1.provice_id = t3.provice_id ",
    query: "SELECT * FROM station WHERE 1=1 ",
    insert: "INSERT INTO station SET ",
    update: "UPDATE station SET ",
    delete: "DELETE FROM station WHERE station_id in ",
    pk: "station_id"
}

module.exports = station;