/**
 * Created by Administrator on 2017/6/23 0023.
 */
var rent_order = {
    rent_order_id : "",    //租箱订单号
    user_id : "",              //用户编号
    rent_use:"",                   //用途
    number:"",               //租用数量
    rent_time:"",            //租用时长
    start_time:"",           //起租时间
    rent_total_price : "",  //租箱总价
    rent_actual_total_price:"",     //实价
    highway_long1: "",      //启程里程
    hignway_price1:"",       //运费1
    highway_long2: "",      //终点里程
    hignway_price2:"",       //运费2
    path_id:"",               //铁路路线
    railway_price:"",        //运费
    order_total_price:"",         //订单总价
    consigner_address:"",         //发货地址
    consignee_address:"",         //收货地址
    order_state:"",                //订单状态
    order_time:"",                 //下单时间
    admin_id:"",                   //订单客服联系人
    queryById: "SELECT t1.*,t2.user_name,t3.path_name,t4.admin_name FROM rent_order t1 INNER JOIN users t2 ON t1.user_id = t2.user_id INNER JOIN  path t3 ON t1.path_id = t3.path_id INNER JOIN  admin t4 ON t1.admin_id = t4.admin_id",
    query: "SELECT * FROM rent_order WHERE 1=1 ",
    insert: "INSERT INTO rent_order SET ",
    update: "UPDATE rent_order SET ",
    delete: "DELETE FROM rent_order WHERE rent_order_id in ",
    pk: "rent_order_id"
}

module.exports = rent_order;