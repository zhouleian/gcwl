/**
 * Created by Administrator on 2017/6/23 0023.
 */
var driver_tasks = {
    driver_id : "",    //司机编号
    task_id : "",              //订单号
    task_valid : "",  //状态
    queryById: "SELECT t1.*,t2.task_name ,t3.driver_name  FROM driver_tasks t1 INNER JOIN transport_tasks t2 ON t1.task_id = t2.task_id INNER JOIN  driver t3 ON t1.driver_id = t3.driver_id ",
    query: "SELECT * FROM driver_tasks WHERE 1=1 ",
    insert: "INSERT INTO driver_tasks SET ",
    update: "UPDATE driver_tasks SET ",
    delete: "DELETE FROM driver_tasks WHERE task_id in ",
    pk: "task_id"
}

module.exports = driver_tasks;