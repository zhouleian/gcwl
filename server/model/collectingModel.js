/**
 * Created by zhangyi on 2017/6/23 0023.
 */
var collecting = {
    asphalt_order_id : "",    //代采订单编号
    user_id : "",  //用户编号
    buy_time : "",  //采购时间
    order_time:"",     //下单时间
    admin_id: "",      //客服
    colle_total_price:"",       //报价
    colle_comment:"",    //备注
    queryById: "SELECT t1.*,t2.user_name ,t3.admin_name FROM collecting_demand t1 INNER JOIN users t2 ON t1.user_id = t2.user_id INNER JOIN  admin t3 ON t1.admin_id = t3.admin_id ",
    query: "SELECT * FROM collecting_demand WHERE 1=1 ",
    insert: "INSERT INTO collecting_demand SET ",
    update: "UPDATE collecting_demand SET ",
    delete: "DELETE FROM collecting_demand WHERE asphalt_order_id in ",
    pk: "asphalt_order_id"
}

module.exports = collecting;