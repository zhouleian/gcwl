/**
 * Created by Administrator on 2017/6/29 0029.
 */
var supplier = {
    supplier_id : "",    //供应商编号
    longitude : "",  //经度
    latitude:"", //纬度
    provice_id:"",   //省
    city_id:"",   //市
    supplier_phone1:"",   //电话
    supplier_phone2:"",   //电话
    supplier_name:"",   //供应商名称
    queryById:"SELECT t1.*,t2.city_name ,t3.provice_name  FROM supplier t1 INNER JOIN city t2 ON t1.city_id = t2.city_id INNER JOIN  provice t3 ON t1.provice_id = t3.provice_id ",
    query: "SELECT * FROM supplier WHERE 1=1 ",
    insert: "INSERT INTO supplier SET ",
    update: "UPDATE supplier SET ",
    delete: "DELETE FROM supplier WHERE supplier_id in ",
    pk: "supplier_id"
}

module.exports = supplier;