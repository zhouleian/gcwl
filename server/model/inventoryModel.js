/**
 * Created by Administrator on 2017/6/29 0029.
 */
var inventory = {
    in_id : "",    //主键
    product_id : "",  //产品编号
    product_num : "",  //产品数量
    supplier_id:"", //供应商编号
    supplier_time:"", //供应时间
    queryById:"SELECT t1.*,t2.product_name ,t3.supplier_name  FROM inventory t1 INNER JOIN product t2 ON t1.product_id = t2.product_id INNER JOIN  supplier t3 ON t1.supplier_id = t3.supplier_id ",
    query: "SELECT * FROM inventory WHERE 1=1 ",
    insert: "INSERT INTO inventory SET ",
    update: "UPDATE inventory SET ",
    delete: "DELETE FROM inventory WHERE in_id in ",
    pk: "in_id"
}

module.exports = inventory;