/**
 * Created by Zhangyi on 2017/7/6 0023.
 */
var productCon = {
    table_id : "",    //编号
    product_order_id : "",  //产品订单号
    container_id : "",  //集装箱编号
    queryById: "SELECT t1.*,t2.container_name,t2.container_state, t3.order_state FROM container_product_order_relation t1 INNER JOIN container t2 ON t1.container_id = t2.container_id INNER JOIN product_order t3 ON t1.product_order_id = t3.product_order_id ",
    insertById: "INSERT INTO container_product_order_relation ",
    query: "SELECT * FROM container_product_order_relation WHERE 1=1 ",
    insert: "INSERT INTO container_product_order_relation SET ",
    update: "UPDATE container_product_order_relation SET ",
    delete: "DELETE FROM container_product_order_relation WHERE table_id in ",
    pk: "table_id"
}

module.exports = productCon;