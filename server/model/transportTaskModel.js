/**
 * Created by Administrator on 2017/6/29 0029.
 */
var transport_tasks = {
    task_id : "",    //任务编号
    task_name : "",  //任务名称
    box_num : "",  //箱子数量
    task_time:"", //任务时间
    startpoint : "",  //起点
    endpoint:"",      //终点
    task_price:"", //报价
    city_id:"",      //城市
    task_state:"", //状态
    order_type:"",      //订单类型
    order_id:"", //订单号
    queryById:"SELECT t1.*,t2.city_name,t3.station_name FROM transport_tasks t1 INNER JOIN city t2 ON t1.city_id = t2.city_id INNER JOIN station t3 ON t1.startpoint= t3.station_id",
    query: "SELECT * FROM transport_tasks WHERE 1=1 ",
    insert: "INSERT INTO transport_tasks SET ",
    update: "UPDATE transport_tasks SET ",
    delete: "DELETE FROM transport_tasks WHERE task_id in ",
    pk: "task_id"
}

module.exports = transport_tasks;