/**
 * Created by Administrator on 2017/6/29 0029.
 */
var tender = {
    tender_id : "",    //投标编号
    provice_id : "",  //省
    city_id : "",  //市
    title:"", //题目
    content : "",  //内容
    state:"",      //状态
    publish_time:"", //发布时间
    queryById:"SELECT t1.*,t2.city_name ,t3.provice_name  FROM tender t1 INNER JOIN city t2 ON t1.city_id = t2.city_id INNER JOIN  provice t3 ON t1.provice_id = t3.provice_id ",
    query: "SELECT * FROM tender WHERE 1=1 ",
    insert: "INSERT INTO tender SET ",
    update: "UPDATE tender SET ",
    delete: "DELETE FROM tender WHERE tender_id in ",
    pk: "tender_id"
}

module.exports = tender;