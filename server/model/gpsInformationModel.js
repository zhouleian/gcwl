/**
 * Created by Administrator on 2017/6/29 0029.
 */
var gps_information = {
    information_id : "",    //信息编号
    gps_id : "",  //gps编号
    time:"", //时间
    longitude : "",  //经度
    latitude:"", //纬度
    temperature:"",   //温度
    queryById:"SELECT t1.*,t2.gps_name  FROM gps_information t1 INNER JOIN gps t2 ON t1.gps_id = t2.gps_id",
    query: "SELECT * FROM gps_information WHERE 1=1 ",
    insert: "INSERT INTO gps_information SET ",
    update: "UPDATE gps_information SET ",
    delete: "DELETE FROM gps_information WHERE information_id in ",
    pk: "information_id"
}

module.exports = gps_information;