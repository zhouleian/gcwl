/**
 * Created by zhangyi on 2017/7/6 0023.
 */
var productOrderInf = {
    table_id : "",    //主键
    product_order_id : "",  //订单
    product_id: "",  //产品编号
    product_num:"",     //数量
    price: "",  //价格
    actual_price:"",     //实际价格
    queryById: "SELECT t3.* ,t4.product_name,t5.user_name  FROM product_order_relation  t3 INNER JOIN  product t4 ON t3.product_id = t4.product_id  INNER JOIN (SELECT t1.product_order_id ,t2.user_name FROM product_order t1  INNER JOIN users t2 ON t1.user_id = t2.user_id) t5 ON t3.product_order_id = t5.product_order_id ",
    query: "SELECT * FROM product_order_relation WHERE 1=1 ",
    insert: "INSERT INTO product_order_relation SET ",
    update: "UPDATE product_order_relation SET ",
    delete: "DELETE FROM product_order_relation WHERE table_id in ",
    pk: "table_id"
}

module.exports = productOrderInf;