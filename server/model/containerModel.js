/**
 * Created by Administrator on 2017/6/29 0029.
 */
var container = {
    container_id : "",    //container编号
    container_name : "",  //名字
    container_state:"",      //状态
    gps_id:"",               //GPS编号
    price:"",                //租价
    queryById:"SELECT t1.*,t2.gps_name  FROM container t1 LEFT JOIN gps t2 ON t1.gps_id = t2.gps_id",
    query: "SELECT * FROM container WHERE 1=1 ",
    insert: "INSERT INTO container SET ",
    update: "UPDATE container SET ",
    delete: "DELETE FROM container WHERE container_id in ",
    pk: "container_id"
}

module.exports = container;