/**
 * Created by Administrator on 2017/6/23 0023.
 */
var admin = {
    admin_id : "",    //管理员编号
    admin_name : "",  //管理员名
    password : "",  //密码
    role_id:"",     //管理员角色
    tel1: "",      //手机
    tel2:"",       //电话
    queryRole:"SELECT * FROM admin_role WHERE 1=1 ",
    queryByRoleId: "SELECT t1.*,t2.role_name FROM admin t1, admin_role t2 WHERE t1.role_id = t2.role_id ",
    query: "SELECT * FROM admin WHERE 1=1 ",
    selectByAdminName:"SELECT * FROM admin WHERE 1=1 ",
    insert: "INSERT INTO admin SET ",
    update: "UPDATE admin SET ",
    delete: "DELETE FROM admin WHERE admin_id in ",
    pk: "admin_id"
}

module.exports = admin;
var db = require('../database.js');


db.mysqlPool.getConnection(function(err, connection) {
    //根据用户名得到用户信息
    admin.getAdminByAdminName = function getAdminByName(admin_name, callback) {

        var getAdminByAdminName_Sql = "SELECT * FROM admin WHERE admin_name = ?";

        connection.query(getAdminByAdminName_Sql, [admin_name], function (err, result) {
            if (err) {
                console.log("getAdminByAdminName Error: " + err.message);
                return;
            }

            connection.release();
            console.log("invoked[getAdminByAdminName]");
            callback(err,result);
        });
    };

});
