/**
 * Created by Administrator on 2017/6/29 0029.
 */
var highway = {
    long_id : "",    //里程编号
    highway_long : "",  //里程段 取得最大
    price : "",  //价格
    query: "SELECT * FROM highway_price WHERE 1=1 ",
    insert: "INSERT INTO highway_price SET ",
    update: "UPDATE highway_price SET ",
    delete: "DELETE FROM highway_price WHERE long_id in ",
    pk: "long_id"
}

module.exports = highway;