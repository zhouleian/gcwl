/**
 * Created by Administrator on 2017/6/29 0029.
 */
var message = {
    msg_id : "",    //信息编号
    msg_type : "",  //信息类型
    msg_title:"", //题目
    msg_content : "",  //内容
    msg_time:"", //发布时间
    msg_picture:"",   //图片（图片路径）
    query: "SELECT * FROM message WHERE 1=1 ",
    insert: "INSERT INTO message SET ",
    update: "UPDATE message SET ",
    delete: "DELETE FROM message WHERE msg_id in ",
    pk: "msg_id"
}

module.exports = message;