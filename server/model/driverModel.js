/**
 * Created by Administrator on 2017/6/23 0023.
 */
var driver = {
    driver_id : "",    //司机编号
    driver_name : "",  //司机真实名
    password : "",  //密码
    id_card:"",     //司机身份证信息
    driverlogin_name: "", //登录名称
    driver_phone:"",      //联系方式
    address:"", //司机所在地详细地址
    license:"",           //驾驶证（图片）
    truck_model:"",               //车辆型号
    truck_id:"",              //牌照信息
    truck_pic:"",     //车辆图片
    truck_state:"", // 身份状态
    provice_id:"", //省份id
    city_id:"", //城市id
    queryById:"SELECT t1.*,t2.city_name ,t3.provice_name  FROM driver t1 INNER JOIN city t2 ON t1.city_id = t2.city_id INNER JOIN  provice t3 ON t1.provice_id = t3.provice_id ",
    query: "SELECT * FROM driver WHERE 1=1 ",
    insert: "INSERT INTO driver SET ",
    update: "UPDATE driver SET ",
    delete: "DELETE FROM driver WHERE driver_id in ",
    pk: "driver_id"
}

module.exports = driver;