/**
 * Created by Administrator on 2017/6/29 0029.
 */
var product = {
    product_id : "",    //产品编号
    product_name : "",  //产品名
    product_introduce:"", //产品介绍
    product_picture:"",           //产品图片（图片路径）
    price:"",               //产品价格
    real_price:"",              //产品真实价格
    query: "SELECT * FROM product WHERE 1=1 ",
    insert: "INSERT INTO product SET ",
    update: "UPDATE product SET ",
    delete: "DELETE FROM product WHERE product_id in ",
    pk: "product_id"
}

module.exports = product;