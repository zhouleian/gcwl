/**
 * Created by Zhangyi on 2017/6/23 0023.
 */
var city= {
    city_id : "",    //编号
    city_name : "",  //名
    provice_id : "",  //省
    queryById: "SELECT t1.*,t2.provice_name FROM city t1, provice t2 WHERE t1.provice_id = t2.provice_id ",
    queryProvice: "SELECT * FROM provice WHERE 1=1 ",
    query: "SELECT * FROM city WHERE 1=1 ",
    insert: "INSERT INTO city SET ",
    update: "UPDATE city SET ",
    delete: "DELETE FROM city WHERE city_id in ",
    pk: "city_id"
}

module.exports = city;