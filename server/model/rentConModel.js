/**
 * Created by Zhangyi on 2017/7/6 0023.
 */
var rentCon = {
    table_id : "",    //编号
    rent_order_id : "",  //租型订单
    container_id : "",  //集装箱编号
    queryById: "SELECT t1.*,t2.container_name,t2.container_state,t3.order_state FROM rent_container_relation t1 INNER JOIN container t2 ON t1.container_id = t2.container_id INNER JOIN rent_order t3 ON t1.rent_order_id = t3.rent_order_id ",
    insertById: "INSERT INTO rent_container_relation ",
    query: "SELECT * FROM rent_container_relation WHERE 1=1 ",
    insert: "INSERT INTO rent_container_relation SET ",
    update: "UPDATE rent_container_relation SET ",
    delete: "DELETE FROM rent_container_relation WHERE table_id in ",
    pk: "table_id"
}

module.exports = rentCon;