/**
 * Created by Administrator on 2017/6/29 0029.
 */
var path = {
    path_id : "",    //路线编号
    path_name : "",  //路线名称
    start_station_id:"", //起点火车站编号
    end_station_id : "",  //终点火车站编号
    path_long:"", //路线长度
    path_time:"",   //花费时间
    money1:"",      //印花税
    money2:"",      //电气附加费
    money3:"",      //铁路建设基金
    money4:"",      //到站取送车费
    money5:"",      //西平运费（西）
    money6:"",      //西平运费（兰）
    money7:"",      //到站装卸费
    money8:"",      //运费
    total_momey:"",      //总运费
    queryByStationId: "SELECT t1.*,t2.station_name AS sstation_name,t3.station_name AS estation_name FROM path t1 INNER JOIN station t2 ON t1.start_station_id = t2.station_id INNER JOIN  station t3 ON t1.end_station_id = t3.station_id ",
    query: "SELECT * FROM path WHERE 1=1 ",
    insert: "INSERT INTO path SET ",
    update: "UPDATE path SET ",
    delete: "DELETE FROM path WHERE path_id in ",
    pk: "path_id"
}

module.exports = path;