/**
 * Created by zhangyi on 2017/6/23 0023.
 */
var collectingOrder = {
    table_id : "",    //主键
    asphalt_order_id : "",  //代采订单
    product_id: "",  //产品编号
    asphalt_num:"",     //数量
    queryById: "SELECT t4.table_id,t4.asphalt_num,t5.*,t6.* FROM asphalt_collecting t4 INNER JOIN (SELECT t1.asphalt_order_id,t2.user_name ,t3.admin_name FROM collecting_demand t1 INNER JOIN users t2 ON t1.user_id = t2.user_id INNER JOIN  admin t3 ON t1.admin_id = t3.admin_id ) t5 on t4.asphalt_order_id = t5.asphalt_order_id INNER JOIN product t6 ON t4.product_id = t6.product_id  ",
    query: "SELECT * FROM asphalt_collecting WHERE 1=1 ",
    insert: "INSERT INTO asphalt_collecting SET ",
    update: "UPDATE asphalt_collecting SET ",
    delete: "DELETE FROM asphalt_collecting WHERE table_id in ",
    pk: "table_id"
}

module.exports = collectingOrder;