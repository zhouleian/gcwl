/**
 * Created by Administrator on 2017/6/30 0030.
 */
var transport_order = {
    transport_order_id : "",    //物流订单编号
    user_id : "",               //用户编号
    highway_long1:"",           //起点公路运输里程
    highway_price1:"",           //起点公路运费
    highway_long2:"",          //终点公路运输里程
    highway_price2:"",           //终点公路运费
    path_id:"",               //运输路线编号
    railway_price:"",              //铁路运费
    order_total_price:"",         //订单总价
    consigner_address:"",         //发货地址
    consignee_address:"",         //收货地址
    order_state:"",                //订单状态
    order_time:"",                 //下单时间
    delivery_time:"",             //发货时间
    admin_id:"",                   //订单客服联系人
    queryById: "SELECT t1.*,t2.user_name,t3.path_name,t4.admin_name FROM transport_order t1 INNER JOIN users t2 ON t1.user_id = t2.user_id INNER JOIN  path t3 ON t1.path_id = t3.path_id INNER JOIN  admin t4 ON t1.admin_id = t4.admin_id",
    query: "SELECT * FROM transport_order WHERE 1=1 ",
    insert: "INSERT INTO transport_order SET ",
    update: "UPDATE transport_order SET ",
    delete: "DELETE FROM transport_order WHERE transport_order_id in ",
    pk: "transport_order_id"
}

module.exports = transport_order;