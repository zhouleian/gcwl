/**
 * Created by Administrator on 2017/6/29 0029.
 */
var newTransportDao = require('../dao/newTransportDao.js');

//查询客户新增线路
exports.query = function(data, callback){
    newTransportDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询客户新增线路
exports.queryById = function(data, callback){
    newTransportDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    newTransportDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增客户新增线路
exports.insert = function(data, callback){
    newTransportDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改客户新增线路
exports.update = function(data, callback){
    newTransportDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除客户新增线路
exports.delete = function(data, callback){
    newTransportDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};