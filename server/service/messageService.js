/**
 * Created by Administrator on 2017/6/29 0029.
 */
var messageDao = require('../dao/messageDao.js');

//查询信息
exports.query = function(data, callback){
    messageDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    messageDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增信息
exports.insert = function(data, callback){
    messageDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改信息
exports.update = function(data, callback){
    messageDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除信息
exports.delete = function(data, callback){
    messageDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};