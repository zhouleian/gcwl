/**
 * Created by Zhangyi on 2017/7/6 0023.
 */

var productConDao = require('../dao/productConDao.js');

//查询产品集装箱关系
exports.queryById = function(data, callback){
    productConDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询产品集装箱关系
exports.query = function(data, callback){
    productConDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询产品集装箱关系
exports.query2 = function(data, callback){
    productConDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增产品集装箱关系
exports.insert = function(data, callback){
    productConDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//批量插入集装箱产品关系
exports.insert2 = function(data, callback){
    productConDao.insert2(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改产品集装箱关系
exports.update = function(data, callback){
    productConDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改集装箱运输状态空
exports.update2 = function(data, callback){
    productConDao.update2(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改集装箱租赁状态为运输中
exports.update3 = function(data, callback){
    productConDao.update3(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};


//删除产品集装箱关系
exports.delete = function(data, callback){
    productConDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};