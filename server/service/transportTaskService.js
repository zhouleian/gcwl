/**
 * Created by Administrator on 2017/6/29 0029.
 */
var transportTaskDao = require('../dao/transportTaskDao.js');

//查询运输任务订单
exports.query = function(data, callback){
    transportTaskDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询运输任务订单
exports.queryById = function(data, callback){
    transportTaskDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询运输任务订单
exports.query2 = function(data, callback){
    transportTaskDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增运输任务订单
exports.insert = function(data, callback){
    transportTaskDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改运输任务订单
exports.update = function(data, callback){
    transportTaskDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除运输任务订单
exports.delete = function(data, callback){
    transportTaskDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};