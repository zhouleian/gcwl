/**
 * Created by Administrator on 2017/6/29 0029.
 */
var gpsDao = require('../dao/gpsDao.js');

//查询GPS
exports.query = function(data, callback){
    gpsDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    gpsDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增GPS
exports.insert = function(data, callback){
    gpsDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改GPS
exports.update = function(data, callback){
    gpsDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除GPS
exports.delete = function(data, callback){
    gpsDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};