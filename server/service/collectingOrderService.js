/**
 * Created by zhangyi on 2017/6/29 0029.
 */
var collectingOrderDao = require('../dao/collectingOrderDao.js');

//查询代采订单详细信息
exports.query = function(data, callback){
    collectingOrderDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询代采订单详细信息
exports.queryById = function(data, callback){
    collectingOrderDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询代采订单详细信息
exports.query2 = function(data, callback){
    collectingOrderDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增代采订单详细信息
exports.insert = function(data, callback){
    collectingOrderDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改代采订单详细信息
exports.update = function(data, callback){
    collectingOrderDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除代采订单详细信息
exports.delete = function(data, callback){
    collectingOrderDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};