/**
 * Created by Administrator on 2017/6/29 0029.
 */
var driverDao = require('../dao/driverDao.js');

//查询司机
exports.query = function(data, callback){
    driverDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询司机
exports.queryById = function(data, callback){
    driverDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询用户
exports.query2 = function(data, callback){
    driverDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    driverDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增司机
exports.insert = function(data, callback){
    driverDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改司机
exports.update = function(data, callback){
    driverDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除司机
exports.delete = function(data, callback){
    driverDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};