/**
 * Created by Administrator on 2017/6/29 0029.
 */
var supplierDao = require('../dao/supplierDao.js');

//查询代理商信息
exports.query = function(data, callback){
    supplierDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询代理商信息
exports.queryById = function(data, callback){
    supplierDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};


//条件查询
exports.query2 = function(data, callback){
    supplierDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增代理商信息
exports.insert = function(data, callback){
    supplierDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改代理商信息
exports.update = function(data, callback){
    supplierDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除代理商信息
exports.delete = function(data, callback){
    supplierDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};