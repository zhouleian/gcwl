/**
 * Created by zhangyi on 2017/7/6 0029.
 */
var productOrderInfDao = require('../dao/productOrderInfDao.js');

//查询产品订单详细信息
exports.query = function(data, callback){
    productOrderInfDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询产品订单详细信息
exports.queryById = function(data, callback){
    productOrderInfDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询产品订单详细信息
exports.query2 = function(data, callback){
    productOrderInfDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增产品订单详细信息
exports.insert = function(data, callback){
    productOrderInfDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改产品订单详细信息
exports.update = function(data, callback){
    productOrderInfDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除产品订单详细信息
exports.delete = function(data, callback){
    productOrderInfDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};