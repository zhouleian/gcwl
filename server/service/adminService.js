/**
 * Created by Administrator on 2017/6/23 0023.
 */

var adminDao = require('../dao/adminDao.js');

//查询管理员Role
exports.queryRole = function(data, callback){
    adminDao.queryRole(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};


//查询管理员
exports.queryByRoleId = function(data, callback){
    adminDao.queryByRoleId(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询管理员
exports.query = function(data, callback){
    adminDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询管理员
exports.query2 = function(data, callback){
    adminDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增管理员
exports.insert = function(data, callback){
    adminDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改管理员
exports.update = function(data, callback){
    adminDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除管理员
exports.delete = function(data, callback){
    adminDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};