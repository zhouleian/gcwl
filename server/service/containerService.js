/**
 * Created by Administrator on 2017/6/29 0029.
 */
var containerDao = require('../dao/containerDao.js');

//查询集装箱
exports.query = function(data, callback){
    containerDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询集装箱
exports.queryById = function(data, callback){
    containerDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增集装箱
exports.insert = function(data, callback){
    containerDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//条件查询用户
exports.query2 = function(data, callback){
    containerDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//修改集装箱
exports.update = function(data, callback){
    containerDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除集装箱
exports.delete = function(data, callback){
    containerDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};