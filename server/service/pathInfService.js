/**
 * Created by zhangyi on 2017/7/6 0029.
 */
var pathInfDao = require('../dao/pathInfDao.js');

//查询路线详细信息
exports.query = function(data, callback){
    pathInfDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询路线详细信息
exports.queryById = function(data, callback){
    pathInfDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询路线详细信息
exports.query2 = function(data, callback){
    pathInfDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增路线详细信息
exports.insert = function(data, callback){
    pathInfDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//批量插入站点路线关系
exports.insert2 = function(data, callback){
    pathInfDao.insert2(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改路线详细信息
exports.update = function(data, callback){
    pathInfDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除路线详细信息
exports.delete = function(data, callback){
    pathInfDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};