/**
 * Created by Administrator on 2017/6/29 0029.
 */
var transportDao = require('../dao/transportDao.js');

//查询运输订单
exports.query = function(data, callback){
    transportDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询运输订单
exports.queryById = function(data, callback){
    transportDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询运输订单
exports.query2 = function(data, callback){
    transportDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增运输订单
exports.insert = function(data, callback){
    transportDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改运输订单
exports.update = function(data, callback){
    transportDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除运输订单
exports.delete = function(data, callback){
    transportDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};