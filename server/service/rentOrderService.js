/**
 * Created by Administrator on 2017/6/29 0029.
 */
var rentOrderDao = require('../dao/rentOrderDao.js');

//查询租箱订单
exports.query = function(data, callback){
    rentOrderDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询租箱订单
exports.queryById = function(data, callback){
    rentOrderDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    rentOrderDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增租箱订单
exports.insert = function(data, callback){
    rentOrderDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改租箱订单
exports.update = function(data, callback){
    rentOrderDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除租箱订单
exports.delete = function(data, callback){
    rentOrderDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};