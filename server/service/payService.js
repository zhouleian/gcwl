/**
 * Created by payistrator on 2017/6/23 0023.
 */

var payDao = require('../dao/payDao.js');

//查询付款信息
exports.queryById = function(data, callback){
    payDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询付款信息
exports.query = function(data, callback){
    payDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询付款信息
exports.query2 = function(data, callback){
    payDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增付款信息
exports.insert = function(data, callback){
    payDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改付款信息
exports.update = function(data, callback){
    payDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除付款信息
exports.delete = function(data, callback){
    payDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};