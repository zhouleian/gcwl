/**
 * Created by Administrator on 2017/6/29 0029.
 */
var tenderDao = require('../dao/tenderDao.js');

//查询招标信息
exports.query = function(data, callback){
    tenderDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询招标信息
exports.queryById = function(data, callback){
    tenderDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询运输订单
exports.query2 = function(data, callback){
    tenderDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增招标信息
exports.insert = function(data, callback){
    tenderDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改招标信息
exports.update = function(data, callback){
    tenderDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除招标信息
exports.delete = function(data, callback){
    tenderDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};