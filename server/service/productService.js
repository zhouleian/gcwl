/**
 * Created by Administrator on 2017/6/29 0029.
 */
var productDao = require('../dao/productDao.js');

//查询产品
exports.query = function(data, callback){
    productDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    productDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增产品
exports.insert = function(data, callback){
    productDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改产品
exports.update = function(data, callback){
    productDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除产品
exports.delete = function(data, callback){
    productDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};