/**
 * Created by Administrator on 2017/6/29 0029.
 */
var pathDao = require('../dao/pathDao.js');

//查询铁路路线
exports.query = function(data, callback){
    pathDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询铁路路线
exports.queryByStationId = function(data, callback){
    pathDao.queryByStationId(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    pathDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增铁路路线
exports.insert = function(data, callback){
    pathDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改铁路路线
exports.update = function(data, callback){
    pathDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除铁路路线
exports.delete = function(data, callback){
    pathDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};