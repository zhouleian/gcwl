/**
 * Created by Zhangyi on 2017/7/6 0023.
 */

var rentConDao = require('../dao/rentConDao.js');

//查询集装箱租赁
exports.queryById = function(data, callback){
    rentConDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询集装箱租赁
exports.query = function(data, callback){
    rentConDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询集装箱租赁
exports.query2 = function(data, callback){
    rentConDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增集装箱租赁
exports.insert = function(data, callback){
    rentConDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//批量插入集装箱租赁
exports.insert2 = function(data, callback){
    rentConDao.insert2(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改集装箱租赁
exports.update = function(data, callback){
    rentConDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改集装箱租赁状态为空
exports.update2 = function(data, callback){
    rentConDao.update2(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改集装箱租赁状态为租赁中
exports.update3 = function(data, callback){
    rentConDao.update3(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除集装箱租赁
exports.delete = function(data, callback){
    rentConDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};