/**
 * Created by Administrator on 2017/6/29 0029.
 */
var gpsInformationDao = require('../dao/gpsInformationDao.js');

//查询物流信息
exports.query = function(data, callback){
    gpsInformationDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询物流信息
exports.queryById = function(data, callback){
    gpsInformationDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    gpsInformationDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增物流信息
exports.insert = function(data, callback){
    gpsInformationDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改物流信息
exports.update = function(data, callback){
    gpsInformationDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除物流信息
exports.delete = function(data, callback){
    gpsInformationDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};