/**
 * Created by Administrator on 2017/6/29 0029.
 */
var driverOrderDao = require('../dao/driverOrderDao.js');

//查询司机订单
exports.query = function(data, callback){
    driverOrderDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询司机订单
exports.queryById = function(data, callback){
    driverOrderDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询用户
exports.query2 = function(data, callback){
    driverOrderDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增司机订单
exports.insert = function(data, callback){
    driverOrderDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改司机订单
exports.update = function(data, callback){
    driverOrderDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除司机订单
exports.delete = function(data, callback){
    driverOrderDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};