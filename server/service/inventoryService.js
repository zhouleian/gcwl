/**
 * Created by Administrator on 2017/6/29 0029.
 */
var inventoryDao = require('../dao/inventoryDao.js');

//查询供应商库存信息
exports.query = function(data, callback){
    inventoryDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询供应商库存信息
exports.queryById = function(data, callback){
    inventoryDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询运输订单
exports.query2 = function(data, callback){
    inventoryDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增供应商库存信息
exports.insert = function(data, callback){
    inventoryDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改供应商库存信息
exports.update = function(data, callback){
    inventoryDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除供应商库存信息
exports.delete = function(data, callback){
    inventoryDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};