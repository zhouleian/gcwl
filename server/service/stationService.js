/**
 * Created by Zhangyi on 2017/6/29 0029.
 */
var stationDao = require('../dao/stationDao.js');

//查询站点信息
exports.query = function(data, callback){
    stationDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询站点信息
exports.queryById = function(data, callback){
    stationDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询运输订单
exports.query2 = function(data, callback){
    stationDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增站点信息
exports.insert = function(data, callback){
    stationDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改站点信息
exports.update = function(data, callback){
    stationDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除站点信息
exports.delete = function(data, callback){
    stationDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};