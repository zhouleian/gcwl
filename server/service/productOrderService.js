/**
 * Created by Administrator on 2017/6/29 0029.
 */
var productOrderDao = require('../dao/productOrderDao.js');

//查询产品订单
exports.query = function(data, callback){
    productOrderDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询产品订单
exports.queryById = function(data, callback){
    productOrderDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    productOrderDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增产品订单
exports.insert = function(data, callback){
    productOrderDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改产品订单
exports.update = function(data, callback){
    productOrderDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除产品订单
exports.delete = function(data, callback){
    productOrderDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};