/**
 * Created by Administrator on 2017/6/29 0029.
 */
var highwayDao = require('../dao/highwayDao.js');

//查询公路计费
exports.query = function(data, callback){
    highwayDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.query2 = function(data, callback){
    highwayDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增公路计费
exports.insert = function(data, callback){
    highwayDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改公路计费
exports.update = function(data, callback){
    highwayDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除公路计费
exports.delete = function(data, callback){
    highwayDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};