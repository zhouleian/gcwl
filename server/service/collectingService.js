/**
 * Created by zhangyi on 2017/6/29 0029.
 */
var collectingDao = require('../dao/collectingDao.js');

//查询代采订单
exports.query = function(data, callback){
    collectingDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询代采订单
exports.queryById = function(data, callback){
    collectingDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询代采订单
exports.query2 = function(data, callback){
    collectingDao.query2(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增代采订单
exports.insert = function(data, callback){
    collectingDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改代采订单
exports.update = function(data, callback){
    collectingDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除代采订单
exports.delete = function(data, callback){
    collectingDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};