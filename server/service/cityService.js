/**
 * Created by Administrator on 2017/6/29 0029.
 */
var cityDao = require('../dao/cityDao.js');

//查询city
exports.query = function(data, callback){
    cityDao.query(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//查询provice
exports.queryProvice = function(data, callback){
    cityDao.queryProvice(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//条件查询
exports.queryById = function(data, callback){
    cityDao.queryById(data, function(err, results){
        if(err){
            callback(true);
            return;
        }
        callback(false, results);
    });
};

//新增city
exports.insert = function(data, callback){
    cityDao.insert(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//修改city
exports.update = function(data, callback){
    cityDao.update(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};

//删除city
exports.delete = function(data, callback){
    cityDao.delete(data, function(err){
        if(err){
            callback(true);
            return;
        }
        callback(false);
    });
};