var express = require('express');
var productService = require('../server/service/productService.js');
var admin = require("../server/model/adminModel.js");
var router = express.Router();
var formidable = require('formidable');
var fs = require('fs');
var AVATAR_UPLOAD_FOLDER = '/avatar/';



router.get('/login', function(req, res, next) {
  res.render('login',{msg:''});
});
router.get('/', function(req, res, next) {
  res.render('login',{msg:''});
});

router.get('/indexerror',function(req,res,next){
    res.render('login',{msg:'用户名或密码有误'});
});

router.get("/logout",function(req,res){    // 到达 /logout 路径则登出， session中admin,error对象置空，并重定向到根路径
    req.session.admin = null;
    req.session.error = null;
    res.render("login",{msg:''});
});

router.get('/index', function(req, res, next) {
  var admins = req.session.admin;

  if(admins){
        res.render('index',{msg:''});
   }else{
        res.render('login', {msg:'请先登录'});
   }
});




module.exports = router;
