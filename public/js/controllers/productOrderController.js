/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';


function ProductOrderCtrl ($scope, $http, $modal) {
    console.log("加载产品订单管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/productOrder/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'product_order_id', displayName:'订单号',width:60},
            {field:'user_name', displayName:'用户',width:60},
            {field:'product_total_price', displayName:'产品总价',width:80},
            {field:'product_actual_total_price', displayName:'产品实价',width:80},
            {field:'highway_long1', displayName:'起点里程',width:80},
            {field:'highway_price1', displayName:'起点运费',width:80},
            {field:'highway_long2', displayName:'终点里程',width:80},
            {field:'highway_price2', displayName:'终点运费',width:80},
            {field:'path_name', displayName:'铁路路线',width:100},
            {field:'railway_price', displayName:'铁路运费',width:80},
            {field:'order_total_price', displayName:'订单总价',width:110},
            {field:'consigner_address', displayName:'发货地址',width:80},
            {field:'consignee_address', displayName:'收货地址',width:80},
            {field:'order_state', displayName:'订单状态',width:80},
            {field:'order_time', displayName:'下单时间',width:100},
            {field:'delivery_time', displayName:'发货时间',width:100},
            {field:'admin_name', displayName:'客服',width:80}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var productOrder_query = $scope.productOrder_query;
            var data = {product_order_id:productOrder_query,user_name:productOrder_query,order_state:productOrder_query};
            $http({
                method: 'POST',
                url: '/productOrder/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/product/productOrderModel.html",
            controller: 'ProductOrderInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/product/productOrderModel.html",
            controller: 'ProductOrderUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["product_order_id"]);
        }
        $http({
            method: 'POST',
            url: '/productOrder/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

    //订单详细信息
    $scope.order = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/product/productOrderInf.html",
            controller: 'ProductOrderInfCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

}

/**
 * 产品订单新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ProductOrderInsertCtrl($scope, $modalInstance, $http,grid){
    $http({
        method: 'POST',
        url: '/admin/query',
        data:{role_id:4}
    }).success(function(results){
        $scope.admin =results;
    });
    $http({
        method: 'POST',
        url: '/path/query'
    }).success(function(results){
        $scope.path_id =results;
    });

    $scope.product_order = {};
    $scope.ok = function () {
        $scope.product_order.delivery_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/productOrder/insert',
            data: $scope.product_order
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 产品订单更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ProductOrderUpdateCtrl($scope, $modalInstance, $http, grid) {
  //  $http({
  //      method: 'POST',
 //       url: '/path/query'
 //   }).success(function(results){
 //       $scope.path_id =results;
 //   })
    $http({
        method: 'POST',
        url: '/admin/query',
        data:{role_id:4}
    }).success(function(results){
        $scope.admin =results;
    });

    $http({
        method: 'POST',
        url: '/productOrder/query',
        data: {product_order_id: grid.gridOptions.selectedItems[0].product_order_id}
    }).success(function (results) {
        $scope.product_order = {};
        for (var key in results[0]) {
            $scope.product_order[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $scope.product_order.delivery_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/productOrder/update',
            data: $scope.product_order
        }).success(function (results) {
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

