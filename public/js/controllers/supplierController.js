/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';

/**
 * 管理员控制
 * @param {[type]} $scope [description]
 * @param {[type]} $http  [description]
 * @param {[type]} $modal [description]
 */
function SupplierCtrl ($scope, $http, $modal) {
    console.log("加载代理商信息管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/supplier/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'supplier_id', displayName:'供应商编码'},
            {field:'supplier_name', displayName:'供应商名称'},
            {field:'provice_name', displayName:'所属省'},
            {field:'city_name', displayName:'所属市'},
            {field:'longitude', displayName:'经度'},
            {field:'latitude', displayName:'纬度'},
            {field:'supplier_phone1', displayName:'联系电话'},
            {field:'supplier_phone2', displayName:'手机'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var supplier_query = $scope.supplier_query;
            var data = {supplier_name:supplier_query,provice_name:supplier_query,city_name:supplier_query};
            $http({
                method: 'POST',
                url: '/supplier/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/message/supplierModel.html",
            controller: 'SupplierInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/message/supplierModel.html",
            controller: 'SupplierUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["supplier_id"]);
        }
        $http({
            method: 'POST',
            url: '/supplier/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 代理商信息新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function SupplierInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    };

    $scope.supplier = {};
    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/supplier/insert',
            data: $scope.supplier
        }).success(function(results){

            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 代理商信息更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function SupplierUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $http({
        method: 'POST',
        url: '/city/query'
    }).success(function(results){
        $scope.city=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    };


    $http({
        method: 'POST',
        url: '/supplier/query',
        data: {supplier_id: grid.gridOptions.selectedItems[0].supplier_id}
    }).success(function (results) {
        $scope.supplier = {};
        for (var key in results[0]) {
            $scope.supplier[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/supplier/update',
            data: $scope.supplier
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

