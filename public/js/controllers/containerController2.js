/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';

/**
 * 管理员控制
 * @param {[type]} $scope [description]
 * @param {[type]} $http  [description]
 * @param {[type]} $modal [description]
 */
function ContainerCtrl2 ($scope, $http, $modal) {
    console.log("加载空的集装箱...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        var data = {container_state:'空'};
        $http({
            method: 'POST',
            url: '/container/query2',
            data:data
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'container_id', displayName:'集装箱编码'},
            {field:'container_name', displayName:'集装箱名称'},
            {field:'container_state', displayName:'状态'},
            {field:'gps_id', displayName:'GPS编码'},
            {field:'gps_name', displayName:'GPS名称'},
            {field:'price', displayName:'租价'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var container_query = $scope.container_query;
            var data = {container_name:container_query,container_state:container_query,gps_id:container_query,gps_name:container_query};
            $http({
                method: 'POST',
                url: '/container/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/container/containerModel.html",
            controller: 'ContainerInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/container/containerModel.html",
            controller: 'ContainerUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["container_id"]);
        }
        $http({
            method: 'POST',
            url: '/container/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

    //集装箱分配给租箱订单
    $scope.rentManage = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/rent/rentConModel2.html",
            controller: 'RentManageCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });

    };

    //集装箱分配给产品订单
    $scope.pManage = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/product/productConModel2.html",
            controller: 'PManageCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });

    };

}

/**
 * 集装箱新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ContainerInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/gps/query'
    }).success(function(results){
        $scope.gps=results;
    });
    $scope.container = {};
    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/container/insert',
            data: $scope.container
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 集装箱更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ContainerUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/gps/query'
    }).success(function(results){
        $scope.gps=results;
    });

    $http({
        method: 'POST',
        url: '/container/query',
        data: {container_id: grid.gridOptions.selectedItems[0].container_id}
    }).success(function (results) {
        $scope.container = {};
        for (var key in results[0]) {
            $scope.container[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/container/update',
            data: $scope.container
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 集装箱分配租箱订单的控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function RentManageCtrl($scope, $modalInstance, $http, grid) {
    var selectedItems = grid.gridOptions.selectedItems;
    var ids = [];
    for(var i = 0; i < selectedItems.length; i++){
       ids.push(selectedItems[i]["container_id"]);
    }
    $scope.rentManage = {};
    $scope.ok = function () {
        var data1 = ids;
        var data2;
        ids[ids.length] = $scope.rentManage.rent_order_id;
        data2 = ids;
        $http({
            method: 'POST',
            url: '/rentCon/insert2',
            data:  data2
        }).success(function () {
            //改变集装箱状态为租赁中
            $http({
                method: 'POST',
                url: '/rentCon/update3',
                data: data1
            }).success(function (results) {
                //刷新列表
                grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
                $modalInstance.close();
                alert(results.msg);
            });
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 集装箱分配产品订单的控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function PManageCtrl($scope, $modalInstance, $http, grid) {
    var selectedItems = grid.gridOptions.selectedItems;
    var ids = [];
    for(var i = 0; i < selectedItems.length; i++){
        ids.push(selectedItems[i]["container_id"]);
    }
    $scope.pManage = {};
    $scope.ok = function () {
        var data3 = ids;
        var data4;
        ids[ids.length] = $scope.pManage.product_order_id;
        data4 = ids;
        $http({
            method: 'POST',
            url: '/productCon/insert2',
            data:  data4
        }).success(function () {
            //改变集装箱状态为租赁中
            $http({
                method: 'POST',
                url: '/productCon/update3',
                data: data3
            }).success(function (results) {
                //刷新列表
                grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
                $modalInstance.close();
                alert(results.msg);
            });
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

