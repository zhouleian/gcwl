/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';

/**
 * 管理员控制
 * @param {[type]} $scope [description]
 * @param {[type]} $http  [description]
 * @param {[type]} $modal [description]
 */
function PathCtrl ($scope, $http, $modal) {
    console.log("加载铁路路线管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/path/queryByStationId'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'path_id', displayName:'路线编码',width:80},
            {field:'path_name', displayName:'路线名称',width:80},
            {field:'sstation_name', displayName:'起点站',width:80},
            {field:'estation_name', displayName:'终点站',width:80},
            {field:'path_long', displayName:'里程',width:60},
            {field:'path_time', displayName:'运到期限/天',width:100},
            {field:'money1', displayName:'印花税',width:80},
            {field:'money2', displayName:'电气化附加费',width:100},
            {field:'money3', displayName:'铁路建设基金',width:100},
            {field:'money4', displayName:'到站取送车费',width:100},
            {field:'money5', displayName:'西平运费(西)',width:100},
            {field:'money6', displayName:'西平运费(兰)',width:100},
            {field:'money7', displayName:'到站装卸费',width:100},
            {field:'money8', displayName:'运费',width:80},
            {field:'total_money', displayName:'总运费/箱',width:80}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var path_query = $scope.path_query;
            var data = {path_name:path_query,sstation_name:path_query,estation_name:path_query};
            $http({
                method: 'POST',
                url: '/path/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/path/pathModel.html",
            controller: 'PathInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/path/pathModel.html",
            controller: 'PathUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["path_id"]);
        }
        $http({
            method: 'POST',
            url: '/path/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

    //路线详细信息
    $scope.pathInf = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条线路");
            return;
        }
        $modal.open({
            templateUrl: "/views/path/pathInf.html",
            controller: 'PathInfCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //路线站点增加信息
    $scope.insert2 = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条线路");
            return;
        }
        var id = selectedItems[0].path_id;
        $modal.open({
            templateUrl: "/views/path/pathStationModel.html",
            controller: 'InsertCtrl2',
            resolve:{
                grid: function(){
                    var path = {path_id:id};
                    return path; }
            }
        });
    };

}

/**
 * 铁路路线新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function PathInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/station/query',
    }).success(function(results){
        $scope.station =results;
    });
    $scope.path = {};
    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/path/insert',
            data: $scope.path
        }).success(function(results){

            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 铁路路线更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function PathUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/station/query',
    }).success(function(results){
        $scope.station =results;
    });

    $http({
        method: 'POST',
        url: '/path/query',
        data: {path_id: grid.gridOptions.selectedItems[0].path_id}
    }).success(function (results) {
        $scope.path = {};
        for (var key in results[0]) {
            $scope.path[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/path/update',
            data: $scope.path
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}


function InsertCtrl2($scope, $modalInstance,grid,$http){
    $http({
        method: 'POST',
        url: '/station/query'
    }).success(function(results){
        $scope.station =results;
    });
    $scope.input = function(){
        var html ="<div class='form-group'><label class='col-md-2 control-label' for='path_order'>站点排序</label>";
        html+= "<div class='col-sm-2'> <input class='form-control'> </div>";
        html+= "<label class='col-md-2 control-label' for='station_id'>站点名称</label> <div class='col-sm-6'>";
        html+= "<select  class='form-control'><option ng-repeat='x in station' value='{{x.station_id}}' ng-bind='x.station_name'></option> </select></div> </div>";
        angular.element(".station").append(html);
        $http({
            method: 'POST',
            url: '/station/query'
        }).success(function(results){
            $scope.station1 =results;
        });
    };
    $scope.ok = function () {
        var input = $('form .form-control');
        var arr =[];
       for(var i=0;i<input.length;i++){
           arr.push(input[i].value);
       }
        arr.push(grid.path_id);
        $http({
            method: 'POST',
            url: '/pathInf/insert2',
            data: arr
        }).success(function (results) {
            //刷新列表
            $modalInstance.close();
            alert(results.msg);
        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

