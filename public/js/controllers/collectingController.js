/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';


function CollectingCtrl ($scope, $http, $modal) {
    console.log("加载代采订单管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/collecting/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'asphalt_order_id', displayName:'订单号'},
            {field:'user_name', displayName:'用户'},
            {field:'buy_time', displayName:'采购时间'},
            {field:'order_time', displayName:'下单时间'},
            {field:'colle_total_price', displayName:'报价'},
            {field:'colle_comment', displayName:'备注'},
            {field:'admin_name', displayName:'客服'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var collecting_query = $scope.collecting_query;
            var data = {asphalt_order_id:collecting_query,user_name:collecting_query,admin_name:collecting_query};
            $http({
                method: 'POST',
                url: '/collecting/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/collecting/collectingModel.html",
            controller: 'CollectingInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/collecting/collectingModel.html",
            controller: 'CollectingUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["asphalt_order_id"]);
        }
        $http({
            method: 'POST',
            url: '/collecting/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

    //订单详细信息
    $scope.order = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/collecting/collectingOrder.html",
            controller: 'CollectingOrderCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

}

/**
 * 代采订单新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function CollectingInsertCtrl($scope, $modalInstance, $http, grid){
    $scope.collecting_demand = {};
    $scope.ok = function () {
        $scope.collecting_demand.buy_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/collecting/insert',
            data: $scope.collecting_demand
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 代采订单更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function CollectingUpdateCtrl($scope, $modalInstance, $http, grid) {

    $http({
        method: 'POST',
        url: '/collecting/query',
        data: {asphalt_order_id: grid.gridOptions.selectedItems[0].asphalt_order_id}
    }).success(function (results) {
        $scope.collecting_demand = {};
        for (var key in results[0]) {
            $scope.collecting_demand[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $scope.collecting_demand.buy_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/collecting/update',
            data: $scope.collecting_demand
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

//订单信息的增删改

