/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';


function TransportTaskCtrl ($scope, $http, $modal) {
    console.log("加载运输任务订单管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/transportTask/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'task_id', displayName:'任务号'},
            {field:'task_name', displayName:'任务名称'},
            {field:'box_num', displayName:'箱子数量'},
            {field:'task_time', displayName:'任务时间'},
            {field:'station_name', displayName:'起点'},
            {field:'endpoint', displayName:'终点'},
            {field:'task_price', displayName:'报价'},
            {field:'city_name', displayName:'城市'},
            {field:'task_state', displayName:'任务状态'},
            {field:'order_type', displayName:'承运订单类型'},
            {field:'order_id', displayName:'订单号'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var task_query = $scope.task_query;
            var data = {city_name:task_query,order_id:task_query,order_type:task_query};
            $http({
                method: 'POST',
                url: '/transportTask/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/message/transportTaskModel.html",
            controller: 'TransportTaskInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/message/transportTaskModel.html",
            controller: 'TransportTaskUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["task_id"]);
        }
        $http({
            method: 'POST',
            url: '/transportTask/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 运输任务订单新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function TransportTaskInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/city/query'
    }).success(function(results){
        $scope.city = results;
    });
    $http({
        method: 'POST',
        url: '/station/query'
    }).success(function(results){
        $scope.station = results;
    });
    $scope.transport_tasks = {};
    $scope.ok = function () {
        $scope.transport_tasks.task_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/transportTask/insert',
            data: $scope.transport_tasks
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 运输任务订单更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function TransportTaskUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/city/query'
    }).success(function(results){
        $scope.city= results;
    });
    $http({
        method: 'POST',
        url: '/station/query'
    }).success(function(results){
        $scope.station = results;
    });

    $http({
        method: 'POST',
        url: '/transportTask/query',
        data: {task_id: grid.gridOptions.selectedItems[0].task_id}
    }).success(function (results) {
        $scope.transport_tasks = {};
        for (var key in results[0]) {
            $scope.transport_tasks[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $scope.transport_tasks.task_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/transportTask/update',
            data: $scope.transport_tasks
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

