/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';


function RentOrderCtrl ($scope, $http, $modal) {
    console.log("加载租箱订单管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/rentOrder/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'rent_order_id', displayName:'订单号',width:80},
            {field:'user_name', displayName:'用户',width:80},
            {field:'rent_use', displayName:'用途',width:50},
            {field:'number', displayName:'数量',width:50},
            {field:'rent_time', displayName:'时长',width:50},
            {field:'start_time', displayName:'起租',width:100},
            {field:'rent_total_price', displayName:'租箱总价',width:100},
            {field:'rent_actual_total_price', displayName:'租箱实价',width:100},
            {field:'highway_long1', displayName:'起点里程',width:100},
            {field:'highway_price1', displayName:'起点运费',width:100},
            {field:'highway_long2', displayName:'终点里程',width:100},
            {field:'highway_price2', displayName:'终点运费',width:100},
            {field:'path_name', displayName:'铁路路线',width:100},
            {field:'railway_price', displayName:'铁路运费',width:100},
            {field:'order_total_price', displayName:'订单总价',width:110},
            {field:'consigner_address', displayName:'发货地址',width:100},
            {field:'consignee_address', displayName:'收货地址',width:100},
            {field:'order_state', displayName:'订单状态',width:100},
            {field:'order_time', displayName:'下单时间',width:100},
            {field:'admin_name', displayName:'客服',width:70}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var rentOrder_query = $scope.rentOrder_query;
            var data = {rent_order_id:rentOrder_query,user_name:rentOrder_query,order_state:rentOrder_query};
            $http({
                method: 'POST',
                url: '/rentOrder/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/rent/rentOrderModel.html",
            controller: 'RentOrderInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/rent/rentOrderModel.html",
            controller: 'RentOrderUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["rent_order_id"]);
        }
        $http({
            method: 'POST',
            url: '/rentOrder/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 租箱订单新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function RentOrderInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/path/query'
    }).success(function(results){
        $scope.path =results;
    });
    $scope.rent_order = {};
    $scope.ok = function () {
        $scope.rent_order.start_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/rentOrder/insert',
            data: $scope.rent_order
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 租箱订单更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function RentOrderUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/path/query'
    }).success(function(results){
        $scope.path =results;
    });

    $http({
        method: 'POST',
        url: '/rentOrder/query',
        data: {rent_order_id: grid.gridOptions.selectedItems[0].rent_order_id}
    }).success(function (results) {
        $scope.rent_order = {};
        for (var key in results[0]) {
            $scope.rent_order[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $scope.rent_order.start_time = $('.form_datetime').val();
        $http({
            method: 'POST',
            url: '/rentOrder/update',
            data: $scope.rent_order
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

