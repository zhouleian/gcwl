/**
 * Created by Zhangyi on 2017/7/6 0023.
 */

'use strict';


function ProductOrderInfCtrl ($scope, $http, $modal,grid) {
    console.log("加载产品订单详细信息管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/productOrderInf/query2',
            data:{product_order_id: grid.gridOptions.selectedItems[0].product_order_id}
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });

    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'table_id', displayName:'编号'},
            {field:'product_order_id', displayName:'产品订单'},
            {field:'user_name', displayName:'用户'},
            {field:'product_name', displayName:'产品'},
            {field:'product_num', displayName:'数量/吨'},
            {field:'price', displayName:'总价'},
            {field:'actual_price', displayName:'实际总价'}
            //{field:'admin_name', displayName:'客服'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var productOrderInf_query = $scope.productOrderInf_query;
            var data = {asphalt_order_id:productOrderInf_query,user_name:productOrderInf_query,product_name:productOrderInf_query};
            $http({
                method: 'POST',
                url: '/productOrderInf/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function( ){
        $modal.open({
            templateUrl: "/views/product/productOrderInfModel.html",
            controller: 'ProductOrderInfInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/product/productOrderInfModel.html",
            controller: 'ProductOrderInfUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["table_id"]);
        }
        $http({
            method: 'POST',
            url: '/productOrderInf/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 产品订单详细信息新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ProductOrderInfInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/product/query'
    }).success(function(results){
        $scope.product = results;
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/productOrderInf/insert',
            data: $scope.productOrderInf
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 产品订单详细信息更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function ProductOrderInfUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/product/query'
    }).success(function(results){
        $scope.product = results;
    });
    $http({
        method: 'POST',
        url: '/productOrderInf/query',
        data: {table_id: grid.gridOptions.selectedItems[0].table_id}
    }).success(function (results) {
        $scope.productOrderInf = {};
        for (var key in results[0]) {
            $scope.productOrderInf[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/productOrderInf/update',
            data: $scope.productOrderInf
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

//订单信息的增删改

