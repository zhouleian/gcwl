/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';


function NewTransportCtrl ($scope, $http, $modal) {
    console.log("加载客户新增线路管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/newTransport/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'transport_id', displayName:'编号'},
            {field:'user_name', displayName:'用户'},
            {field:'provice1', displayName:'发货省份'},
            {field:'city1', displayName:'发货城市'},
            {field:'start_station', displayName:'发货站'},
            {field:'start_address', displayName:'发货详细地址'},
            {field:'provice2', displayName:'收货省份'},
            {field:'city2', displayName:'收货城市'},
            {field:'end_station', displayName:'收货站'},
            {field:'end_address', displayName:'收货详细地址'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var newTransport_query = $scope.newTransport_query;
            var data = {start_station:newTransport_query,user_name:newTransport_query,end_station:newTransport_query};
            $http({
                method: 'POST',
                url: '/newTransport/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/message/newTransportModel.html",
            controller: 'NewTransportInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/message/newTransportModel.html",
            controller: 'NewTransportUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["transport_id"]);
        }
        $http({
            method: 'POST',
            url: '/newTransport/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 产品订单新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function NewTransportInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    };
    $scope.change1 = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city1=results;
        });
    };
    $scope.new_transport = {};
    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/newTransport/insert',
            data: $scope.new_transport
        }).success(function(results){
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 产品订单更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function NewTransportUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $http({
        method: 'POST',
        url: '/city/query'
    }).success(function(results){
        $scope.city=results;
        $scope.city1=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    };
    $scope.change1 = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city1=results;
        });
    };
    $http({
        method: 'POST',
        url: '/newTransport/query',
        data: {transport_id: grid.gridOptions.selectedItems[0].transport_id}
    }).success(function (results) {
        $scope.new_transport = {};
        for (var key in results[0]) {
            $scope.new_transport[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/newTransport/update',
            data: $scope.new_transport
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

