/**
 * Created by Administrator on 2017/6/23 0023.
 */


'use strict';

/**
 * 管理员控制
 * @param {[type]} $scope [description]
 * @param {[type]} $http  [description]
 * @param {[type]} $modal [description]
 */

function SelfCtrl2 ($scope, $modal) {
    console.log("加载用户管理...");
    var data = $("#self_id").text();
    $scope.self=function(){
         $modal.open({
             templateUrl: "/views/self/self.html",
             controller: 'Ctrl',
             resolve: {
                 grid: function () {
                     var self = {admin_id: data};
                     return self;}
                         }
                     });
    }
}
function Ctrl($scope,$http,grid,$modal,$modalInstance){
    $http({
        method: 'POST',
        url: '/admin/query',
        data:grid
    }).success(function (results) {
        $scope.self = {};
        for (var key in results[0]) {
            $scope.self[key] = results[0][key];
        }
    });
    //更新
    $scope.update = function(){
        $modal.open({
            templateUrl: "/views/self/selfModel.html",
            controller: 'SelfUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };
    $scope.cancel= $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}


/**
 * 用户更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 * @param {[type]} $window           [description]
 */
function SelfUpdateCtrl($scope, $modalInstance,$window , $http, grid) {
    $scope.reloadRoute = function () {
        $window.location.reload();
    };
    $http({
        method: 'POST',
        url: '/admin/query',
        data: {admin_id: grid.self.admin_id}
    }).success(function (results) {
        $scope.admin = {};
        for (var key in results[0]) {
            $scope.admin[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/admin/update',
            data: $scope.admin
        }).success(function (results) {
            //刷新列表
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}



