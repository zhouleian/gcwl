/**
 * Created by Administrator on 2017/6/23 0023.
 */

'use strict';

/**
 * 管理员控制
 * @param {[type]} $scope [description]
 * @param {[type]} $http  [description]
 * @param {[type]} $modal [description]
 */
function DriverCtrl ($scope, $http, $modal) {
    console.log("加载司机管理...");

    /**
     * 配置ng-grid
     * @type {Object}
     */
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 50, 100],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        $http({
            method: 'POST',
            url: '/driver/queryById'
        }).success(function(largeLoad) {
            var obj = $scope.gridOptions.selectedItems;
            obj.splice(0,obj.length);
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        i18n:'zh-cn',
        enablePaging: true,
        showFooter: true,
        autoScroll:true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            {field:'driver_id', displayName:'司机编码'},
            {field:'driver_name', displayName:'真实名称'},
            {field:'password', displayName:'密码'},
            {field:'id_card', displayName:'身份证号'},
            {field:'driverlogin_name', displayName:'登录名称'},
            {field:'driver_phone', displayName:'联系方式'},
            {field:'address', displayName:'详细地址'},
            {field:'license', displayName:'驾驶证'},//图片
            {field:'truck_model', displayName:'车辆型号'},
            {field:'truck_id', displayName:'车牌号'},
            {field:'truck_pic', displayName:'车辆图片'},
            {field:'truck_state', displayName:'身份审核'},
            {field:'provice_name', displayName:'所在省份'},
            {field:'city_name', displayName:'所在城市'}
        ]
    };

    //条件查询
    $scope.query2 = function( pageSize, page, searchText){
        $scope.getPagedDataAsync2 = function (pageSize, page, searchText) {
            var driver_query = $scope.driver_query;
            var data = {driver_name:driver_query,driverlogin_name:driver_query,truck_id:driver_query};
            $http({
                method: 'POST',
                url: '/driver/query2',
                data:data
            }).success(function(largeLoad) {
                var obj = $scope.gridOptions.selectedItems;
                obj.splice(0,obj.length);
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        };
        $scope.getPagedDataAsync2($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    };

    //新增
    $scope.insert = function(){
        $modal.open({
            templateUrl: "/views/driver/driverModel.html",
            controller: 'DriverInsertCtrl',
            resolve: {
                grid: function(){ return $scope; }
            }
        });
    };

    //更新
    $scope.update = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length != 1){
            alert("请选择一条记录");
            return;
        }
        $modal.open({
            templateUrl: "/views/driver/driverModel.html",
            controller: 'DriverUpdateCtrl',
            resolve:{
                grid: function(){ return $scope; }
            }
        });
    };

    //删除
    $scope.delete = function(){
        var selectedItems = $scope.gridOptions.selectedItems;
        if(selectedItems.length == 0){
            alert("请至少选择一条记录");
            return;
        }
        if(!confirm("删除是不可恢复的，你确认要删除吗？")){
            return;
        }
        var selectedItems = $scope.gridOptions.selectedItems;
        var ids = [];
        for(var i = 0; i < selectedItems.length; i++){
            ids.push(selectedItems[i]["driver_id"]);
        }
        $http({
            method: 'POST',
            url: '/driver/delete',
            data: ids
        }).success(function(results){
            //刷新列表
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            alert(results.msg);
        });
    };

}

/**
 * 司机新增控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function DriverInsertCtrl($scope, $modalInstance, $http, grid){
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    }
    $scope.driver = {};
    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/driver/insert',
            data: $scope.driver
        }).success(function(results){

            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * 司机更新控制
 * @param {[type]} $scope         [description]
 * @param {[type]} $modalInstance [description]
 * @param {[type]} $http          [description]
 * @param {[type]} grid           [description]
 */
function DriverUpdateCtrl($scope, $modalInstance, $http, grid) {
    $http({
        method: 'POST',
        url: '/provice/query'
    }).success(function(results){
        $scope.provice=results;
    });
    $http({
        method: 'POST',
        url: '/city/query'
    }).success(function(results){
        $scope.city=results;
    });
    $scope.change = function(id){
        $http({
            method: 'POST',
            url: '/city/query',
            data:{provice_id:id}
        }).success(function(results){
            $scope.city=results;
        });
    };

    $http({
        method: 'POST',
        url: '/driver/query',
        data: {driver_id: grid.gridOptions.selectedItems[0].driver_id}
    }).success(function (results) {
        $scope.driver = {};
        for (var key in results[0]) {
            $scope.driver[key] = results[0][key];
        }
    });

    $scope.ok = function () {
        $http({
            method: 'POST',
            url: '/driver/update',
            data: $scope.driver
        }).success(function (results) {
            //刷新列表
            grid.getPagedDataAsync(grid.pagingOptions.pageSize, grid.pagingOptions.currentPage);
            $modalInstance.close();
            alert(results.msg);
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

